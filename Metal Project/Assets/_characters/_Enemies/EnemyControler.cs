﻿using UnityEngine;
using MetalProject.Core;
using MetalProject.Combat;
using MetalProject.CommonUtilty;

namespace MetalProject.Characters
{
    public class EnemyControler : Controler2D
    {
        #region SerializedFields

        [Header("Agro Parameters")]
        [SerializeField] float chasingMoveSpeed;
        [SerializeField] float chaseOffsetMin;
        [SerializeField] float chaseOffsetMax;

        [Header("Raycast Parameters")]
        [SerializeField] LayerMask damageAndMovmentRaysLayerMask;
        [SerializeField] LayerMask visionRaysLayerMask;
        [SerializeField] float damageAndMovmentRaysLenght;
        [SerializeField] float visionRaysLenght;

        [Header("Teleport Parameters")]
        [SerializeField] float teleportPositionOffset;
        [SerializeField] float teleportDelay;


        #endregion

        #region Fields
        RaycastControler raycastControler;
        EnemyAnimationManager enemyAnimationManager;
        EnemyCombatControler enemyCombatControler;
        Enemy enemy;
        Player player;

        Vector3 velocity;
        float gravity;

        float pushStartTime;
        float pushDuration;

        float pickedDistance;
        bool distanceWasPicked;

        float lastTimePlayerWasSpoted;

        #endregion

        #region Properties
        public RaycastHit2D GroundHitLeft { get; private set; }
        public RaycastHit2D GroundHitRight { get; private set; }
        public RaycastHit2D FrontHitCheck { get; private set; }
        public RaycastHit2D PlayerVisionHitCheck { get; private set; }

        public float ChasingMoveSpeed
        {
            get { return chasingMoveSpeed; }
            private set { chasingMoveSpeed = value; }
        }

        public float DistanceToPlayerX { get; private set; }
        public float DistanceToPlayerY { get; private set; }
        public Vector2 DirectionToPlayer { get; private set; }
        public bool PlayerInCombatArea { get; private set; }
        public bool PlayerSpoted { get; private set; }

        public CombatArea CombatArea { get; private set; }

        public bool EnemyRecovered { get; private set; }
        public bool GravityAffectsEnemy { get; private set; }

        #endregion

        new void Start()
        {
            base.Start();
            SetupComponents();

            CombatArea.OnPlayerEntered += PlayerEnteredCombatArea;
            enemyAnimationManager.OnEnemyTeleports += PerformTeleport;

            gravity = -56f;
            distanceWasPicked = false;
            SetGravityAffectsEnemy(true);
            SetEnemyRecovered(true);

            chasingMoveSpeed += Random.Range(-1f, 1f); // Randomized speed and teleport delay to give help with overllaping effect
            teleportDelay += Random.Range(-1f, 1f);
        }

        void Update()
        {
            CalculateDistanceToPlayerX();
            CalculateDistanceToPlayerY();
            CalculateDirectionToPlayer();
            SendMovmentCheckRays();
            SendDamageCheckRays();
            SendVisionCheckRays();
            CheckIfEnemyShouldTeleport();
            CheckIfEnemyShouldBeFalling();
            CheckIfEnemyShouldChase();

            if (!distanceWasPicked) { PickDistance(); }
            if (PlayerInCombatArea) { CheckIfPlayerIsInRange(); }
            if ((enemyAnimationManager.GetChasing() || enemyAnimationManager.GetFigthing()) && !enemyCombatControler.EnemyIsAttacking) { DecideOnMovementDirection(); }

            SetVelocityX();
            SetVelocityY();

            Move(velocity * Time.deltaTime);
        }


        #region private methods
        private void SetupComponents()
        {
            raycastControler = GetComponent<RaycastControler>();
            enemyAnimationManager = GetComponent<EnemyAnimationManager>();
            enemyCombatControler = GetComponentInChildren<EnemyCombatControler>();
            player = FindObjectOfType<Player>();
            enemy = GetComponent<Enemy>();
            CombatArea = GetComponentInParent<CombatArea>();
        }
        private void CheckIfEnemyShouldBeFalling()
        {
            if (!collisionControler.Below)
            {
                enemyAnimationManager.SetFalling(true);
                enemy.SetCanBeStuned(true);
            }
            else
            {
                enemyAnimationManager.SetFalling(false);
            }
        }
        private void CheckIfEnemyShouldTeleport()
        {
            if (!PlayerSpoted && PlayerInCombatArea && DistanceToPlayerY > 6 && !enemy.IsDead
                && enemy.EnemyInCombatArea
                && lastTimePlayerWasSpoted + teleportDelay <= Time.time)
            {
                enemyAnimationManager.SetTeleport(true);
            }
        }
        private void CheckIfEnemyShouldChase()
        {
            if (PlayerInCombatArea) { enemyAnimationManager.SetChasing(true); }
            else { enemyAnimationManager.SetChasing(false); }
        }
        private void PickDistance()
        {
            pickedDistance = Random.Range(chaseOffsetMin, chaseOffsetMax);
            distanceWasPicked = true;
        }
        private void CheckIfPlayerIsInRange()
        {
            if (DistanceToPlayerX < pickedDistance)
            {
                enemyAnimationManager.SetFigthing(true);
                enemyAnimationManager.SetChasing(false);
            }
            else
            {
                enemyAnimationManager.SetFigthing(false);
                enemyAnimationManager.SetChasing(true);
            }
        }
        private void PlayerEnteredCombatArea(bool value)
        {
            if (enemyAnimationManager != null)
            {
                if (value)
                {
                    enemyAnimationManager.SetChasing(value);
                    PlayerInCombatArea = value;
                }
                else
                {
                    enemyAnimationManager.SetChasing(value);
                    PlayerInCombatArea = value;
                }
            }
        }

        private void SendMovmentCheckRays()
        {
            GroundHitLeft = Physics2D.Raycast(raycastControler.BottomLeft_OriginPoint, Vector2.down, damageAndMovmentRaysLenght, collisionControler.GetCollisionMask());
            Debug.DrawRay(raycastControler.BottomLeft_OriginPoint, Vector2.down * damageAndMovmentRaysLenght, Color.yellow);

            GroundHitRight = Physics2D.Raycast(raycastControler.BottomRight_OriginPoint, Vector2.down, damageAndMovmentRaysLenght, collisionControler.GetCollisionMask());
            Debug.DrawRay(raycastControler.BottomRight_OriginPoint, Vector2.down * damageAndMovmentRaysLenght, Color.yellow);
        }
        private void SendDamageCheckRays()
        {
            if (MovmentDirection == Vector2.right.x)
            {
                FrontHitCheck = Physics2D.Raycast(raycastControler.Center_OriginPoint, Vector2.right, damageAndMovmentRaysLenght, damageAndMovmentRaysLayerMask);
                Debug.DrawRay(raycastControler.Center_OriginPoint, Vector2.right * damageAndMovmentRaysLenght, Color.green);
            }
            else if (MovmentDirection == Vector2.left.x)
            {
                FrontHitCheck = Physics2D.Raycast(raycastControler.Center_OriginPoint, Vector2.left, damageAndMovmentRaysLenght, damageAndMovmentRaysLayerMask);
                Debug.DrawRay(raycastControler.Center_OriginPoint, Vector2.left * damageAndMovmentRaysLenght, Color.green);
            }
        }
        private void SendVisionCheckRays()
        {
            PlayerVisionHitCheck = Physics2D.Raycast(raycastControler.Center_OriginPoint, DirectionToPlayer, visionRaysLenght, visionRaysLayerMask);
            Debug.DrawRay(raycastControler.Center_OriginPoint, DirectionToPlayer * visionRaysLenght, Color.blue);

            if (PlayerVisionHitCheck)
            {
                if (PlayerVisionHitCheck.collider.gameObject.GetComponent<Player>() != null) { PlayerSpoted = true; }
                else if (PlayerSpoted)
                {
                    PlayerSpoted = false;
                    lastTimePlayerWasSpoted = Time.time;
                }
            }
        }
        private void CalculateDistanceToPlayerX()
        {
            Vector2 temp = player.transform.position;
            Vector2 temp2 = this.transform.position;

            temp.y = 0;
            temp2.y = 0;

            DistanceToPlayerX = Mathf.Abs(Vector2.Distance(temp, temp2));
        }
        private void CalculateDistanceToPlayerY()
        {
            Vector2 temp = player.transform.position;
            Vector2 temp2 = this.transform.position;
            temp.x = 0;
            temp2.x = 0;
            DistanceToPlayerY = Mathf.Abs(Vector2.Distance(temp, temp2));
        }
        private void CalculateDirectionToPlayer()
        {
            DirectionToPlayer = (player.transform.position - this.transform.position).normalized;
        }
        private void DecideOnMovementDirection()
        {
            if (DirectionToPlayer.x < 0)
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
                SetMovmentDirection(Vector2.left.x);
            }
            if (DirectionToPlayer.x > 0)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
                SetMovmentDirection(Vector2.right.x);
            }

            if (((MovmentDirection == Vector2.left.x && !GroundHitLeft) || (MovmentDirection == Vector2.right.x && !GroundHitRight))
                && !enemyAnimationManager.GetFalling())

            {
                enemyAnimationManager.SetFigthing(true);
                enemyAnimationManager.SetAttackNumber(MyEnums.EnemyAttack.Fireball);
            }
        }

        private void PerformTeleport()
        {
            Vector2 temp = player.transform.position;
            if (DirectionToPlayer.x < 0) { temp.x += Random.Range(-teleportPositionOffset, 0f); } //TODO there probalby should be better way for offseting from player position
            else { temp.x += Random.Range(0f, teleportPositionOffset); }
            transform.position = temp;
            enemyAnimationManager.SetTeleport(false);
        }

        protected override void SetVelocityX()
        {
            if (pushStartTime + pushDuration <= Time.time)
            {
                if (EnemyRecovered)
                {
                    if (enemyAnimationManager.GetChasing() && collisionControler.Below && !enemyCombatControler.EnemyIsAttacking) { velocity.x = ChasingMoveSpeed * MovmentDirection; }
                    if (enemyAnimationManager.GetFireBlastCharging()) { velocity.x = 0; }
                    if (enemyAnimationManager.GetFigthing() && !enemyCombatControler.EnemyIsAttacking) { velocity.x = 0; }
                    if (enemyAnimationManager.GetTeleport()) { velocity.x = 0; }
                    if (enemy.IsDead) { velocity.x = 0; }
                }
                else
                {
                    velocity.x = 0;
                }
            }
        }
        protected override void SetVelocityY()
        {
            if (collisionControler.Above) { velocity.y = 0; }
            if (GravityAffectsEnemy && !collisionControler.Below) { velocity.y += gravity * Time.deltaTime; }
        }

        #endregion

        #region Public Methods
        public void SetPushVelocity(Vector2 distance, float pushDirection, float pushDuration, string attakcName)
        {
            pushStartTime = Time.time;
            this.pushDuration = pushDuration;

            if (attakcName == "Stomp")
            {
                if (DirectionToPlayer.x > 0) { velocity.x = -distance.x / pushDuration; }
                else if (DirectionToPlayer.x < 0) { velocity.x = distance.x / pushDuration; }
            }
            else { velocity.x = pushDirection * distance.x / pushDuration; }

            velocity.y = distance.y / pushDuration;

        }
        public void SetGravityAffectsEnemy(bool value)
        {
            GravityAffectsEnemy = value;
        }
        public void SetEnemyRecovered(bool value)
        {
            EnemyRecovered = value;
        }
        public void SetMovmentDirection(float value)
        {
            MovmentDirection = value;
        }
        public void SetAttackMovmentVelocity(float movmentDuration, Vector2 movmentVector)
        {
            velocity.x = (MovmentDirection * (movmentVector.x)) / movmentDuration;
            if (movmentVector.y > 0)
            {
                velocity.y = movmentVector.y / movmentDuration;
            }
        }
        public void ResetAttackMovmentVelocity()
        {
            velocity.x = 0f;
        }

        #endregion

    }
}
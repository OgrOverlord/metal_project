﻿using System.Collections;
using UnityEngine;
using MetalProject.CommonUtilty;

namespace MetalProject.Characters
{
    public class Enemy : MonoBehaviour, ICharacter
    {

        #region Serialzied fields
        [Header("Health")]
        [SerializeField] [Range(100, 500)] int maxHealth;

        [Header("Stamina")]
        [SerializeField] [Range(0, 500)] int maxStamina;
        [SerializeField] [Range(0, 100)] int staminafireBlastThreshold;

        [Header("Other")]
        [SerializeField] bool canBeStuned;
        [SerializeField] int concurentAttacksForBlock;


        #endregion

        #region Properties
        public int MaxHealth
        {
            get { return maxHealth; }
            private set { maxHealth = value; }
        }
        public int CurentHealth { get; private set; }
        public int MaxStamina
        {
            get { return maxStamina; }
            private set { maxStamina = value; }
        }
        public int CurentStamina { get; private set; }

        public bool IsInvincable { get; private set; }
        public bool IsDead { get; private set; }
        public bool CanBeStuned
        {
            get { return canBeStuned; }
            private set { canBeStuned = value; }
        }

        public bool EnemyInCombatArea { get; private set; }

        public int AttacksTaken { get; private set; }
        public int ConcurentAttacksForBlock
        {
            get { return concurentAttacksForBlock; }
            private set { concurentAttacksForBlock = value; }
        }

        #endregion

        #region fields
        private EnemyAnimationManager enemyAnimationManager;
        private EnemyControler enemyControler;

        #endregion

        #region Events and delegates
        public delegate void OnEnemyDiesDelegate(Enemy enemy);
        public event OnEnemyDiesDelegate OnEnemyDies;

        public delegate void OnEnemyHealthModifedDelegate(int value);
        public event OnEnemyHealthModifedDelegate OnEnemyHealthModifed;
        #endregion


        void Start()
        {
            CurentHealth = maxHealth;
            CurentStamina = maxStamina;
            AttacksTaken = 0;
            enemyAnimationManager = GetComponent<EnemyAnimationManager>();
            enemyControler = GetComponent<EnemyControler>();
        }

        void Update()
        {
            if (AttacksTaken >= concurentAttacksForBlock)
            {
                EnemyBlocks();
            }
        }

        public void ModifyHealthBy(int value)
        {
            CurentHealth += value;
            OnEnemyHealthModifed(value);
            enemyAnimationManager.SetChasing(true);
            if (value < 0 && CanBeStuned) { enemyAnimationManager.SetTakingDamage(); }

            if ((float)CurentHealth / (float)MaxHealth <= 0.5f && ConcurentAttacksForBlock == 5)
            {
                ConcurentAttacksForBlock -= 2;
            }
            if (CurentHealth <= 0) { Die(); }
        }
        public void SetHealthTo(int value)
        {
            CurentHealth = value;
        }
        public void ModifyStaminaBy(int value)
        {
            CurentStamina += value;
            if (!IsDead)
            {
                if (CurentStamina <= staminafireBlastThreshold)
                {
                    CurentStamina = maxStamina;
                    StartCharingFireBlastAttack();
                }
            }
        }
        public void Die()
        {
            IsDead = true;
            enemyAnimationManager.SetDying();
            enemyControler.SetEnemyRecovered(true);
            SetIsInvincable(true);
            OnEnemyDies(this);
            StartCoroutine(DestroyEnemyAfterDelay());
        }
        public void SetCanBeStuned(bool value)
        {
            CanBeStuned = value;
        }
        public void SetIsInvincable(bool value)
        {
            // Debug.Log(value);
            IsInvincable = value;
            if (value) { enemyAnimationManager.SetOutlineThicknessShaderProperty(1f); }
            else { enemyAnimationManager.SetOutlineThicknessShaderProperty(0); }
        }
        public void SetEnemyInCombatArea(bool value)
        {
            EnemyInCombatArea = value;
        }
        public void IncrementAttacksTaken()
        {
            AttacksTaken++;
        }
        public void RestAttacksTaken()
        {
            AttacksTaken = 0;
        }

        private void StartCharingFireBlastAttack() //TODO there are a few edge cases when this is working not as intended, enemy can be locked in air invicble, or on the ground locked in charing loop
        {
            enemyAnimationManager.SetFireBlastCharging(true);
            SetCanBeStuned(false);
            SetIsInvincable(true);
        }
        private void EnemyBlocks()
        {
            enemyAnimationManager.SetBlocking();
            AttacksTaken = 0;
            SetCanBeStuned(false);
            SetIsInvincable(true);
        }

        IEnumerator DestroyEnemyAfterDelay()
        {
            yield return new WaitForSeconds(10f);
            Destroy(this.gameObject);
        }


    }

}
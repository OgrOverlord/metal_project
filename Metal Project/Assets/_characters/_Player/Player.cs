﻿using System.Collections;
using UnityEngine;
using MetalProject.Combat;
using MetalProject.Core;
using MetalProject.CommonUtilty;

namespace MetalProject.Characters
{
    [SelectionBase]
    public class Player : MonoBehaviour, ICharacter
    {

        #region SerializeFields
        [SerializeField] [Range(10, 500)] int maxHealth = 100;
        [SerializeField] [Range(10, 500)] int maxStamina = 200;
        [SerializeField] int stamianRegenRate = 10;
        [SerializeField] float staminaRegenFrequency = 0.5f;
        [SerializeField] ExhaustionData exhaustionData;
        #endregion

        #region propeerties
        public int MaxHealth
        {
            get { return maxHealth; }
            private set { maxHealth = value; }
        }
        public int CurentHealth { get; private set; }
        public bool IsInvincable { get; private set; }
        public bool IsDead { get; private set; }

        public int MaxStamina
        {
            get { return maxStamina; }
            private set { maxStamina = value; }
        }
        public int CurentStamina { get; private set; }

        public int StamianRegenRate
        {
            get { return stamianRegenRate; }
            private set { stamianRegenRate = value; }
        }
        public int CurrentStaminaRegenRate { get; private set; }
        public float StaminaRegenFrequency
        {
            get { return staminaRegenFrequency; }
            private set { staminaRegenFrequency = value; }
        }
        public bool StaminaRegenBlocked { get; private set; }
        #endregion

        #region fields
        GameManager gameManager;

        PlayerControler playerControler;
        PlayerAnimationManager playerAnimationManager;
        PlayerShield playerShield;
        StatusEffectController statusEffectController;

        bool staminRecharged;
        float lastStaminaRegenTime;
        #endregion

        #region events
        public delegate void OnPlayerDiedDelegate();
        public event OnPlayerDiedDelegate OnPlayerDied;

        public delegate void OnPlayerGettingHitDelegate(MetalEventArgs args);
        public event OnPlayerGettingHitDelegate OnPlayerGettingHit;
        #endregion

        void Awake()
        {
            IsInvincable = false;

            CurentStamina = MaxStamina;
            CurentHealth = maxHealth;
            CurrentStaminaRegenRate = StamianRegenRate;
            StaminaRegenBlocked = false;

            gameManager = FindObjectOfType<GameManager>();
            playerControler = GetComponent<PlayerControler>();
            playerAnimationManager = GetComponent<PlayerAnimationManager>();
            statusEffectController = GetComponent<StatusEffectController>();
            playerShield = GetComponentInChildren<PlayerShield>();

            playerControler.OnMovmentAbiltyUsed += ModifyStamina;
            playerShield.OnPlayerAttackBlocked += ModifyStamina;
        }
        void Start()
        {
            LoadPlayerData();
            if (PlayerData.PlayerDied) { ResetPlayerStatsOnRespawn(); }
        }
        void Update()
        {
            if (!staminRecharged && !StaminaRegenBlocked) { StaminaRegeneration(); }
            if (CurentStamina >= MaxStamina)
            {
                CurentStamina = MaxStamina;
                staminRecharged = true;
            }
            else
            {
                staminRecharged = false;
            }
        }

        #region public methods
        public void ModifyStaminaBy(int value)
        {
            CurentStamina += value;
        }
        public void ModifyStaminaRechargeRate(int value)
        {
            CurrentStaminaRegenRate = value;
        }
        public void ResetStaminaRechargeRate()
        {
            CurrentStaminaRegenRate = StamianRegenRate;
        }

        public void SetIsInvincable(bool value)
        {
            IsInvincable = value;
        }
        public void SetIsDead(bool value)
        {
            IsDead = value;
            PlayerData.PlayerDied = IsDead;
        }

        public void ModifyHealthBy(int value)
        {
            CurentHealth += value;
            OnPlayerGettingHit(new MetalEventArgs(0.3f,0.2f,1f)); 
            if (value < 0)
            {
                playerAnimationManager.SetTakingDamage();
                playerAnimationManager.SetGrabbing(false);
            }
            if (CurentHealth > MaxHealth) { CurentHealth = MaxHealth; }
            if (CurentHealth <= 0) { Die(); }
        }
        public void SetHealthTo(int value)
        {
            CurentHealth = value;
        }
        public void ResetPlayerStatsOnRespawn()
        {
            CurentStamina = MaxStamina;
            CurentHealth = maxHealth;
            CurrentStaminaRegenRate = StamianRegenRate;
            playerAnimationManager.SetRespawn();
            SetIsDead(false);
            SetIsInvincable(false);
        }
        public void SetStaminaRegenBlocked(bool value)
        {
            StaminaRegenBlocked = value;
        }
        public void Die()
        {
            playerAnimationManager.SetDying();
            PlayerData.ExitAreaId = string.Empty; //Need for settinbg proper looking direction and local scale of the player after respawn
            gameManager.RespawnPlayer(this);
            SetIsDead(true);
        }
        #endregion

        #region private methods
        private void LoadPlayerData()
        {
            if (PlayerData.Health != -1) { CurentHealth = PlayerData.Health; }
        }

        private void ModifyStamina(int value)
        {
            CurentStamina += value;
            if (CurentStamina <= 0)
            {
                CurentStamina = 0;
                playerAnimationManager.SetBlocking(false);
                statusEffectController.AddStatus(new Exhaustion(exhaustionData, this.gameObject));
            }
        }
        private void StaminaRegeneration()
        {
            if (lastStaminaRegenTime + StaminaRegenFrequency <= Time.time)
            {
                ModifyStamina(CurrentStaminaRegenRate);
                lastStaminaRegenTime = Time.time;
            }
        }

        #endregion
    }
}

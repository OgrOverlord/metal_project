﻿using UnityEngine;
using MetalProject.Combat;
using MetalProject.Core;
using MetalProject.CommonUtilty;
using System;

namespace MetalProject.Characters
{
    public class PlayerControler : Controler2D
    {
        #region SerializedFields

        [Header("Jump Parameters")]
        [SerializeField] float jumpHeight;
        [SerializeField] float timeToJumpApex;
        [SerializeField] int availableJumps;

        [Header("Dash Parameters")]
        [SerializeField] float dashRange;
        [SerializeField] float dashTime;
        [SerializeField] float dashCooldown;
        [SerializeField] [Range(-50, 0)] int dashCost;

        [Header("Grab Parameters")]
        [SerializeField] [Range(-50, 0)] int grabInitialCost;
        [SerializeField] [Range(-50, 0)] int grabUpkeepCost;
        [SerializeField] float grabAngleCheckRayLenght;
        [SerializeField] [Range(80, 90)] float grabWallAngleLow;
        [SerializeField] [Range(90, 100)] float grabWallAngleHigh;
        [SerializeField] LayerMask grabRayLayerMask;

        [Header("Stomp Parameters")]
        [SerializeField] float stompVelocity;
        [SerializeField] [Range(-50, 0)] int stompCost;
        [SerializeField] MeleeAttack stompAttackData;

        [Header("AfterImage")]
        [SerializeField] float afterImageDelay;


        [Header("Move Parameters")]
        [SerializeField] float runningSpeed;
        [SerializeField] float sprintSpeed;
        [SerializeField] float sprintTimeThreashold;
        [SerializeField] [Range(-50, 0)] int sprintUpkeepCost;
        [SerializeField] [Range(-50, 0)] int sprintInitialCost;
        [SerializeField] float accelerationTimeAirborne;
        [SerializeField] float accelerationTimeGrounded;
        #endregion

        #region Private Fields

        Vector2 input;
        Vector2 velocity;

        PlayerAnimationManager playerAnimationManager;
        PlayerCombatControler playerCombatControler;
        RaycastControler raycastControler;
        Player player;
        GameManager gameManager;

        float defaultRunningSpeed;
        float defaultSprintSpeed;

        float gravityAceleration;
        float jumpVelocity;
        int numberOfJumpsMade;

        float dashVelocity;
        float dashDirection;
        float timeDashWasUsed;

        bool stomping;
        bool sprinting;

        bool startedRunning;
        float startedRunningTime;
        float velocityXSmothing;

        float lastAfterImageActivationTime;

        bool hardCollisionSpeedReached;
        #endregion

        #region Properties
        public float RunningSpeed
        {
            get { return runningSpeed; }
            set { runningSpeed = value; }
        }
        public float SprintSpeed
        {
            get { return sprintSpeed; }
            set { sprintSpeed = value; }
        }
        public bool GravityAffectsPlayer { get; private set; }

        #endregion

        #region Events

        public delegate void OnPlayerHardCollideDelegate(MetalEventArgs args);
        public event OnPlayerHardCollideDelegate OnPlayerHardCollide;

        public delegate void OnPlayerFSprintingOrRunningDelegate(bool sprinting);
        public event OnPlayerFSprintingOrRunningDelegate OnPlayerSprintingOrRunning;

        public delegate void OnMovmentAbiltyUsedDelegate(int useCost);
        public event OnMovmentAbiltyUsedDelegate OnMovmentAbiltyUsed;

        public delegate void OnInteractionButtonPressedDelegate();
        public event OnInteractionButtonPressedDelegate OnInteractionButtonPressed;
        #endregion

        void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
            gameManager.GameInputMaster.Player.Jump.performed += ctx => Jump();

            gameManager.GameInputMaster.Player.Move.performed += ctx => input = ctx.ReadValue<Vector2>();
            gameManager.GameInputMaster.Player.Move.canceled += ctx => input = Vector2.zero;

            gameManager.GameInputMaster.Player.Dash_Stomp.performed += ctx => Stomp();
            gameManager.GameInputMaster.Player.Dash_Stomp.performed += ctx => Dash();
            gameManager.GameInputMaster.Player.Grab.performed += ctx => Grab();
            gameManager.GameInputMaster.Player.Interaction.performed += ctx => Interaction();
            gameManager.GameInputMaster.Player.Enable();
        }
        new void Start()
        {
            base.Start();

            gravityAceleration = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
            jumpVelocity = Mathf.Abs(gravityAceleration) * timeToJumpApex;
            dashVelocity = dashRange / dashTime;
            defaultRunningSpeed = RunningSpeed;
            defaultSprintSpeed = SprintSpeed;

            stomping = false;
            sprinting = false;
            startedRunning = false;
            GravityAffectsPlayer = true;
            if (PlayerData.ExitAreaId == string.Empty) { MovmentDirection = (int)Vector2.right.x; } //Condtion added for initial spawn

            player = GetComponent<Player>();
            playerAnimationManager = GetComponent<PlayerAnimationManager>();
            playerCombatControler = GetComponentInChildren<PlayerCombatControler>();
            raycastControler = GetComponent<RaycastControler>();
        }
        void Update()
        {
            ChangePlayerLookingDirection();
            SetVelocityX();
            SetVelocityY();
            AdditionalChecksAndSkillReseting();
            Move(velocity * Time.deltaTime);
        }
        void OnDisable()
        {
            gameManager.GameInputMaster.Player.Disable();
        }

        #region Inherted_methods   
        protected override void SetVelocityX()
        {
            if (!playerCombatControler.PlayerIsAttacking)
            {
                Run();
                Sprint();
            }

            //Reset velocity after collision during dash
            if (playerAnimationManager.GetDashing()
                && (collisionControler.Left || collisionControler.Right)
                && Mathf.Abs(velocity.x) > 0)
            {
                velocity.x = 0;
                playerAnimationManager.SetDashing(false);
            }

            //Rest x velocty when doing the stomp or grabbing
            if (stomping || playerAnimationManager.GetGrabbing() || playerAnimationManager.GetBlocking())
            {
                velocity.x = 0f;
            }
        }
        protected override void SetVelocityY()
        {
            //Reset velocity when collided with celling or the ground
            if ((collisionControler.Above || collisionControler.Below) && !playerCombatControler.PlayerIsAttacking)
            {
                velocity.y = 0;
            }

            //Gravity put in condition so it only works when player is not dashing and grabbing
            if (!playerAnimationManager.GetDashing() && !playerAnimationManager.GetGrabbing())
            {
                if (GravityAffectsPlayer) { velocity.y += gravityAceleration * Time.deltaTime; }
                else if (!playerCombatControler.PlayerIsAttacking) { velocity.y = 0; }
            }
            if (playerAnimationManager.GetGrabbing()) { velocity.y = 0; }
            if (playerAnimationManager.GetDashing() && !stomping) { velocity.y = 0; }
            if (playerAnimationManager.GetChargingAttack()) { velocity.y = 0; }
        }

        #endregion

        #region Private_methods
        private void Run()
        {
            if (Mathf.Abs(input.x) >= 0
                && !playerAnimationManager.GetDashing()
                && !playerAnimationManager.GetGrabbing()
                && !playerAnimationManager.GetBlocking()
                && !sprinting)
            {
                if (collisionControler.Below)
                {
                    if (Mathf.Abs(input.x) > 0)
                    {
                        playerAnimationManager.SetRunning(true);
                        if (!startedRunning)
                        {
                            startedRunningTime = Time.time;
                            startedRunning = true;
                        }
                    }
                    else
                    {
                        playerAnimationManager.SetRunning(false);
                    }
                }
                float targetVelocityX = input.x * RunningSpeed;
                velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmothing, (collisionControler.Below) ? accelerationTimeGrounded : accelerationTimeAirborne);

                if (input.x == 0)
                {
                    startedRunning = false;
                }
            }
        }
        private void Sprint()
        {
            if (startedRunningTime + sprintTimeThreashold <= Time.time
                && startedRunning
                && collisionControler.Below
                && player.CurentStamina >= Mathf.Abs(sprintInitialCost))
            {
                //Condtion added so that inital cost is paid only once
                if (!sprinting) { OnMovmentAbiltyUsed(sprintInitialCost); }

                if (input.x == 0) { startedRunning = false; }

                if (Mathf.Abs(input.x) > 0) { playerAnimationManager.SetRunning(true); } //TODO when you are going to have spriting animation adjust this flags
                else { playerAnimationManager.SetRunning(false); }

                sprinting = true;
                OnPlayerSprintingOrRunning(sprinting);

                if (Mathf.Abs(input.x) >= 0  //normal sprint on the ground
                    && !playerAnimationManager.GetDashing()
                    && !playerAnimationManager.GetGrabbing()
                    && sprinting)
                {
                    float targetVelocityX = input.x * SprintSpeed;
                    velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmothing, accelerationTimeGrounded);

                    //condition added so that stamina is used only when player applys input 
                    if (Mathf.Abs(input.x) > 0) { player.ModifyStaminaRechargeRate(sprintUpkeepCost); }
                }
            }
            else if (sprinting && !collisionControler.Below) //player sprinted and jumped, the velocity is kept
            {
                float targetVelocityX = input.x * SprintSpeed;
                velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmothing, accelerationTimeAirborne);
                player.ResetStaminaRechargeRate();
            }
            else  //player falls down after accelarated movment through the air
            {
                sprinting = false;
                OnPlayerSprintingOrRunning(sprinting);
            }
        }

        private void Jump()
        {
            if (numberOfJumpsMade < availableJumps && !playerAnimationManager.GetBlocking())
            {
                velocity.y = jumpVelocity;
                numberOfJumpsMade++;

                if (numberOfJumpsMade == 1)
                {
                    playerAnimationManager.SetPlayerJump();
                }
                else
                {
                    playerAnimationManager.SetPlayerSecondJump();
                }
                playerAnimationManager.SetGrabbing(false);
                Move(velocity * Time.deltaTime);
            }

        }
        private void Stomp()
        {
            if (input.y < 0
                && input.x == 0
                && !collisionControler.Below
                && player.CurentStamina >= Mathf.Abs(stompCost)
                && !stomping)
            {
                velocity.y -= stompVelocity;
                stomping = true;
                OnMovmentAbiltyUsed(stompCost);
                playerCombatControler.SetCurrentAttack(stompAttackData);
                playerAnimationManager.SetGrabbing(false);
                playerAnimationManager.SetStartStomping();
            }
        }
        private void Grab()
        {
            RaycastHit2D grabCheck;
            float wallAngle = 0f;
            if (MovmentDirection == Vector2.right.x) { grabCheck = Physics2D.Raycast(raycastControler.Center_OriginPoint, Vector2.right, grabAngleCheckRayLenght, grabRayLayerMask); }
            else { grabCheck = Physics2D.Raycast(raycastControler.Center_OriginPoint, Vector2.left, grabAngleCheckRayLenght, grabRayLayerMask); }

            if (grabCheck)
            {
                Vector2 normal = grabCheck.normal;
                wallAngle = Vector2.Angle(normal, Vector2.up);
            }

            if ((collisionControler.Left || collisionControler.Right)
                && !playerAnimationManager.GetGrabbing()
                && player.CurentStamina >= Mathf.Abs(grabInitialCost)
                && grabCheck
                && wallAngle > grabWallAngleLow
                && wallAngle < grabWallAngleHigh)
            {
                velocity.y = 0;
                numberOfJumpsMade = 0;

                OnMovmentAbiltyUsed(grabInitialCost);
                player.ModifyStaminaRechargeRate(grabUpkeepCost);
                playerAnimationManager.SetStartGrabbing();
                playerAnimationManager.SetGrabbing(true);
                playerAnimationManager.SetRunning(false);
                sprinting = false;
                playerAnimationManager.SetAirborn(false);
            }
        }
        private void Dash()
        {
            if (Mathf.Abs(input.x) >= 0
                && input.y == 0
                && !playerAnimationManager.GetDashing()
                && timeDashWasUsed + dashCooldown < Time.time
                && player.CurentStamina >= Mathf.Abs(dashCost))
            {
                dashDirection = MovmentDirection;
                if (playerAnimationManager.GetGrabbing()) { dashDirection = Mathf.Sign(input.x); }

                playerCombatControler.SetPlayerIsAttacking(false);
                playerAnimationManager.SetGrabbing(false);
                playerAnimationManager.SetDashing(true);

                player.SetIsInvincable(true);
                velocity.x = dashVelocity * dashDirection;
                timeDashWasUsed = Time.time;
                OnMovmentAbiltyUsed(dashCost);
            }
        }
        private void Interaction()
        {
            OnInteractionButtonPressed();
        }
        private void AdditionalChecksAndSkillReseting()
        {
            // CheckWallTypeAndAngleForGrab();

            //Set Air if player is not grabbing or dashing
            if (!playerAnimationManager.GetGrabbing() && !playerAnimationManager.GetDashing())
            {
                playerAnimationManager.SetAirborn(!collisionControler.Below);
            }

            //Check if player stopped dashing
            if ((timeDashWasUsed + dashTime < Time.time)
                && playerAnimationManager.GetDashing())
            {
                player.SetIsInvincable(false);
                playerAnimationManager.SetDashing(false);
            }
            else if (playerAnimationManager.GetDashing() && lastAfterImageActivationTime + afterImageDelay < Time.time) //Check if it is time to spawn another after image
            {
                var afterImageInstance = PlayerAfterImagePool.Instance.GetFromPool();
                lastAfterImageActivationTime = Time.time;
            }

            //Reset number of jumps and air combo state when hitting the ground
            if (collisionControler.Below)
            {
                numberOfJumpsMade = 0;
                playerCombatControler.SetAirComboPerformed(false);
            }

            //Stomp ends when hitting the ground 
            if (collisionControler.Below
                && stomping)
            {
                stomping = false;
                playerAnimationManager.SetStomp();

                OnPlayerHardCollide(new MetalEventArgs(0.5f, 0.5f, 1f));
            }

            // Shake screen when player collides with the ground with large Y velocity
            if (collisionControler.Below
                && !playerCombatControler.PlayerIsAttacking
                && hardCollisionSpeedReached)
            {
                OnPlayerHardCollide(new MetalEventArgs(0.5f, 0.8f, 1f));
            }
            if (velocity.y <= -stompVelocity) { hardCollisionSpeedReached = true; }
            else { hardCollisionSpeedReached = false; }


            if (player.CurentStamina == 0)
            {
                sprinting = false;
                playerAnimationManager.SetGrabbing(false);
            }

            if (!playerAnimationManager.GetGrabbing()
                && !playerAnimationManager.GetBlocking()
                && !sprinting)
            {
                player.ResetStaminaRechargeRate();
            }
        }
        private void ChangePlayerLookingDirection()
        {
            if (Mathf.Abs(input.x) > 0) //Condition added so that player scale on sprite render object is not set to 0
            {
                if (input.x > 0 && !playerAnimationManager.GetGrabbing()) { MovmentDirection = 1; }
                if (input.x < 0 && !playerAnimationManager.GetGrabbing()) { MovmentDirection = -1; }
            }
            transform.localScale = new Vector3(MovmentDirection, 1, 1);
        }

        #endregion

        #region Public_methods
        public void SetAttackMovmentVelocity(Vector2 distanceToMove, float MovmentDuration = 1f)
        {
            velocity.x = MovmentDirection * distanceToMove.x / MovmentDuration;
            velocity.y = distanceToMove.y / MovmentDuration;
        }
        public void SetGravityAffectsPlayer(bool value)
        {
            GravityAffectsPlayer = value;
        }
        public void SetMovmentSpeedTo(float value)
        {
            RunningSpeed = value;
            SprintSpeed = value;
        }
        public void ResetMovmentSpeedToDefault()
        {
            runningSpeed = defaultRunningSpeed;
            sprintSpeed = defaultSprintSpeed;
        }
        public void ChangePlayerLookingDirection(int value)
        {
            MovmentDirection = value;
            transform.localScale = new Vector3(MovmentDirection, 1, 1);
        }

        public Vector2 GetCurrentDirectionalInput()
        {
            return input;
        }
        #endregion
    }
}

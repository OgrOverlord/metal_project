﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.Combat;

namespace MetalProject.Core
{
    public class ExitArea : MonoBehaviour
    {
        [SerializeField] private LevelData levelToLoad;

        [SerializeField] string id;

        private Transform entryPoint;
        private GameManager gameManager;

        public string Id
        {
            get { return id; }
            private set { id = value; }
        }

        void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
            entryPoint = GetComponentInChildren<Transform>();
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<Player>())
            {
                Player player = collision.GetComponent<Player>();
                PlayerCombatControler playerCombatControler = player.GetComponentInChildren<PlayerCombatControler>();

                PlayerData.Health = player.CurentHealth;
                PlayerData.NormalBullet = playerCombatControler.AvailableBullets[0];
                PlayerData.PiercingBullets = playerCombatControler.AvailableBullets[1];
                PlayerData.ExitAreaId = id;
                PlayerData.EnteredExitArea = true;

                gameManager.LoadScene(levelToLoad.levelName);
            }
        }

        public void SetId(string value)
        {
            Id = value;
        }
    }
}
﻿using UnityEngine;
using NaughtyAttributes;

namespace MetalProject.Core
{
    public class EntryPoint : MonoBehaviour
    {
        [SerializeField] [Range(-1, 1)] int playerMovmentDirection;
        [SerializeField] string id;

        public string Id
        {
            get { return id; }
            private set { id = value; }
        }
        public int PlayerMovmentDirection
        {
            get { return playerMovmentDirection; }
            private set { playerMovmentDirection = value; }
        }

        public void SetId(string value)
        {
            Id = value;
        }
    }
}

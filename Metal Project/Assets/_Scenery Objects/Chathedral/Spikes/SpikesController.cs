﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.Core;
using System.Collections;

namespace MetalProject.Enviroment
{
    public class SpikesController : MonoBehaviour
    {
        [SerializeField] private AudioClip warrningSound;
        [SerializeField] private float activationDelay;

        Animator animator;

        void Start()
        {
            animator = GetComponent<Animator>();
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<Player>())
            {
                AudioManager.Instance.PlaySFX(warrningSound);
                StartCoroutine(ActivateSpikeAfterDelay());
            }
        }

        IEnumerator ActivateSpikeAfterDelay()
        {
            yield return new WaitForSeconds(activationDelay);
            animator.SetTrigger("Activate");
        }


    }
}
﻿using UnityEngine;
using MetalProject.Characters;

namespace MetalProject.Enviroment
{
    public class DamageTriggerControl : MonoBehaviour
    {
        [SerializeField] private int damage;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<Player>())
            {
                Player player = collision.GetComponent<Player>();
                if (!player.IsInvincable) { player.ModifyHealthBy(damage); }
            }
        }
    }
}
﻿using System.Collections.Generic;
using UnityEngine;
using MetalProject.Characters;
using MetalProject.Core;
using MetalProject.UI;

namespace MetalProject.Enviroment
{
    public class CheckpointShrine : MonoBehaviour
    {
        [SerializeField] private GameObject playerSpawner;
        [SerializeField] private ButtonPromptUI buttonPrompt;
        [SerializeField] private bool isInitiallyActive;

        private GameManager gameManager;
        private bool interactionAvilable;
        private bool isActive;

        private List<CheckpointShrine> shrineList = new List<CheckpointShrine>();
        private PlayerControler playerControler;
        private Animator aimator;
        private AudioSource audioSource;

        void Awake()
        {
            interactionAvilable = false;
            isActive = false;
            aimator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
        }
        void Start()
        {
            playerControler = FindObjectOfType<PlayerControler>();
            gameManager = FindObjectOfType<GameManager>();

            SetReferencToOtherShrinesOnLevel();
            CheckIfShrineShouldBeActive();

            playerControler.OnInteractionButtonPressed += SetNewRespawnPoint;
        }
        void Update()
        {
            if (!isActive) { audioSource.Stop(); } //NOTE SFXController changes volume and check if player is within hearing range, this blocks sfx from playing when shrine is inactive
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<Player>()) { interactionAvilable = true; }
            if (interactionAvilable && !isActive && !buttonPrompt.gameObject.activeSelf) { buttonPrompt.gameObject.SetActive(true); }
        }
        void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.GetComponent<Player>()) { interactionAvilable = false; }
            buttonPrompt.gameObject.SetActive(false);
        }

        private void SetReferencToOtherShrinesOnLevel()
        {
            var shrineArray = FindObjectsOfType<CheckpointShrine>();
            foreach (var shrine in shrineArray)
            {
                if (shrine != this) { shrineList.Add(shrine); }
            }
        }
        private void CheckIfShrineShouldBeActive()
        {
            if (isInitiallyActive && !isActive && PlayerData.ActivatedShrinePositionX == 0f) //activates initial shrine for the start of the game
            {
                playerSpawner.transform.position = transform.position;
                PlayerData.ShrineActivated = true;
                PlayerData.ActivatedShrinePositionX = transform.position.x;
                PlayerData.ActivatedShrinePositionY = transform.position.y;
                PlayerData.RespawnLevelIndex = gameManager.ActiveSceneIndex;
                isActive = true;
                aimator.SetBool("Active", true);
            }

            //check if sgrine on level was activate before
            if (PlayerData.ActivatedShrinePositionX == transform.position.x && PlayerData.ActivatedShrinePositionY == transform.position.y && PlayerData.ShrineActivated) //
            {
                isActive = true;
                aimator.SetBool("Active", true);
            }
        }
        private void SetNewRespawnPoint()
        {
            if (interactionAvilable && !isActive)
            {
                playerSpawner.transform.position = transform.position;
                aimator.SetBool("Active", true);
                PlayerData.ShrineActivated = true;
                PlayerData.ActivatedShrinePositionX = transform.position.x;
                PlayerData.ActivatedShrinePositionY = transform.position.y;
                PlayerData.RespawnLevelIndex = gameManager.ActiveSceneIndex;
                buttonPrompt.gameObject.SetActive(false);
                isActive = true;

                foreach (var shrine in shrineList) { shrine.ResetShrine(); } //Dectivate other shrines when a new one is activated        
            }
        }

        public void ResetShrine()
        {
            isActive = false;
            aimator.SetBool("Active", false);
        }

    }
}
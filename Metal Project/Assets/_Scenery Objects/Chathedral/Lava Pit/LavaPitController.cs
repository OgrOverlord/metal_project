﻿using UnityEngine;
using MetalProject.Characters;

namespace MetalProject.Enviroment
{
    public class LavaPitController : MonoBehaviour
    {
        [SerializeField] StatusEffectData slowdownData;
        [SerializeField] StatusEffectData burningData;

        private float slowLastApplyTime;
        private float slowApplyFrequency;

        private float burningLastApplyTime;
        private float burningApplyFrequency;

        void Start()
        {
            slowLastApplyTime = 0f;
            burningLastApplyTime = 0f;

            slowApplyFrequency = slowdownData.duration;
            burningApplyFrequency = burningData.duration;
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.GetComponent<StatusEffectController>() != null)
            {
                if (slowLastApplyTime + slowApplyFrequency <= Time.time)
                {
                    collision.GetComponent<StatusEffectController>().AddStatus(new Slowdown(slowdownData, collision.gameObject));
                    slowLastApplyTime = Time.time;
                }
                if (burningLastApplyTime + burningApplyFrequency <= Time.time)
                {
                    collision.GetComponent<StatusEffectController>().AddStatus(new Burning(burningData, collision.gameObject));
                    burningLastApplyTime = Time.time;
                }
            }
        }
    }
}
﻿using UnityEditor;
using UnityEngine;
using MetalProject.Combat;

[CustomEditor(typeof(Bullet))]
public class BulletEditor : Editor
{

    public override void OnInspectorGUI()
    {
        var myScript = target as Bullet;
        string[] layers = new string[] 
        {
            LayerMask.LayerToName(0),
            LayerMask.LayerToName(1),
            LayerMask.LayerToName(2),
            LayerMask.LayerToName(3),
            LayerMask.LayerToName(4),
            LayerMask.LayerToName(5),
            LayerMask.LayerToName(6),
            LayerMask.LayerToName(7),
            LayerMask.LayerToName(8),
            LayerMask.LayerToName(9),
            LayerMask.LayerToName(10),
            LayerMask.LayerToName(12),
            LayerMask.LayerToName(13),
            LayerMask.LayerToName(14),
            LayerMask.LayerToName(15),
            LayerMask.LayerToName(16),
            LayerMask.LayerToName(17),
            LayerMask.LayerToName(18),
            LayerMask.LayerToName(19),
            LayerMask.LayerToName(20),
        }; //TODO in case of problems with layers this should probalby be update in a more organic way


        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Bullet Layers", EditorStyles.boldLabel);
        myScript.layerMaskHit = EditorGUILayout.MaskField("Layer Mask Hit", myScript.layerMaskHit, layers);
        myScript.layerMaskDamages = EditorGUILayout.MaskField("Layer Mask Damages", myScript.layerMaskDamages, layers);

        EditorGUILayout.Space();

        myScript.pierces = GUILayout.Toggle(myScript.pierces, "Pierces");

        if (myScript.pierces)
        {
            myScript.piercingRange = EditorGUILayout.Slider("Piercing Range", myScript.piercingRange, 0f, 15f);
            myScript.targetsToPierce = EditorGUILayout.IntSlider("Targets To Pierce", myScript.targetsToPierce, 0, 5);
        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Bullet Data", EditorStyles.boldLabel);

        myScript.damage = EditorGUILayout.IntSlider("Damage", myScript.damage, -200, 0);
        myScript.maxNumber = EditorGUILayout.IntField("Max Number", myScript.maxNumber);
        myScript.startingNumber = EditorGUILayout.IntField("Starting Number", myScript.startingNumber);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Bullet presentation", EditorStyles.boldLabel);

        myScript.bulletSplash = (GameObject)EditorGUILayout.ObjectField("Bullet Splash", myScript.bulletSplash, typeof(GameObject), false);
        myScript.bulletSound = (AudioClip)EditorGUILayout.ObjectField("Bullet Splash", myScript.bulletSound, typeof(AudioClip), false);
        myScript.bulletSprite = (Sprite)EditorGUILayout.ObjectField("Bullet Splash", myScript.bulletSprite, typeof(Sprite), false);

    }

}

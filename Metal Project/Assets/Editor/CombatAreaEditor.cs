﻿using UnityEngine;
using UnityEditor;
using MetalProject.Combat;
using System;

[CustomEditor(typeof(CombatArea))]
public class CombatAreaEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI(); //TODO consider to display a proper parameters for selected condition, maybe Naughty Attributes can help or you can do it manually

        CombatArea area = (CombatArea)target;

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Generate New ID")) { GenerateGUID(area); }
        if (GUILayout.Button("Rest ID")) { SetEmptyGUID(area); }

        EditorGUILayout.EndHorizontal();
    }


    private void GenerateGUID(CombatArea area)
    {
        area.SetAreaID(Guid.NewGuid().ToString());
    }
    private void SetEmptyGUID(CombatArea area)
    {
        area.SetAreaID(Guid.Empty.ToString());
    }
}

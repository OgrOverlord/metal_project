﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MetalProject.Core;
using MetalProject.Characters;
using System;

namespace MetalProject.Enviroment
{
    public class MovingPlatformControler : Controler2D
    {
        [SerializeField] Vector2[] PlatformWaypoints;

        int waypointIndex = 0;
        bool targetSet = false;
        float timeToReachWaypoint = 5f;
        float distanceToNearestWaypoint = float.MaxValue;
        Vector2 targetWaypoint = Vector2.zero;

        Vector2 velocity;

        List<PassangerMovment> passangerMovments;

        new void Start()
        {
            base.Start();
        }

        void Update()
        {
            translation = CalculatePlatformMovment();
            collisionControler.ResetCollisionInfoAndRaycastOrigins();
            CalculatePassangerMovment(translation);

            MovePassangers(true);
            Move(translation);
            MovePassangers(false);
        }

        private Vector2 CalculatePlatformMovment()
        {
            SelectFirstTargetWaypoint();
            CheckIfPlatformArivedAtWaypoint();

            if (targetSet) //TODO consider making velocity dependend on changing time value to keep the same speed on diffrent distances
            {
                SetVelocityX();
                SetVelocityY();
                targetSet = false;
            }

            return velocity * Time.deltaTime;
        }
        private void MovePassangers(bool beforMovePlatform)
        {
            foreach (PassangerMovment passenger in passangerMovments)
            {
                if (passenger.moveBeforePlatform == beforMovePlatform)
                {
                    passenger.passangerTransform.GetComponent<Controler2D>().Move(passenger.translation, passenger.standingOnPlatform);
                }
            }
        }
        private void CalculatePassangerMovment(Vector2 translation)
        {
            HashSet<Transform> movedPassangers = new HashSet<Transform>(); //Allows to let platform know if cenrtain object was already moved by the platform
            passangerMovments = new List<PassangerMovment>(); // List of all characters on the platform 

            float directionX = Mathf.Sign(translation.x);
            float directionY = Mathf.Sign(translation.y);

            if (translation.y != 0)
            {
                passangerMovments = collisionControler.PushPasangerUp(translation, movedPassangers, directionY, passangerMovments);
            }

            if (translation.x != 0)
            {
                passangerMovments = collisionControler.PushPassangerToSides(translation, movedPassangers, directionX, directionY, passangerMovments);
            }

            if (directionY == -1 || translation.y == 0 && translation.x != 0)
            {
                passangerMovments = collisionControler.MovePassangerDownWithPlatform(translation, movedPassangers, directionY, passangerMovments);
            }
        }

        private void CheckIfPlatformArivedAtWaypoint()
        {
            if (Vector2.Distance(this.transform.position, targetWaypoint) < 0.2f)
            {
                waypointIndex++;
                if (waypointIndex == PlatformWaypoints.Length)
                {
                    waypointIndex = 0;
                    targetWaypoint = PlatformWaypoints[waypointIndex];
                }
                else
                {
                    targetWaypoint = PlatformWaypoints[waypointIndex];
                }
                targetSet = true;
            }
        }
        private void SelectFirstTargetWaypoint()
        {
            if (targetWaypoint == Vector2.zero)
            {
                for (int i = 0; i < PlatformWaypoints.Length; i++)
                {
                    if (distanceToNearestWaypoint > Vector2.Distance(transform.position, PlatformWaypoints[i]))
                    {
                        distanceToNearestWaypoint = Vector2.Distance(transform.position, PlatformWaypoints[i]);
                        targetWaypoint = PlatformWaypoints[i];
                        waypointIndex = i;
                        targetSet = true;
                    }
                }
            }
        }

        public override void Move(Vector2 translation, bool standingOnPlatform = false)
        {
            transform.Translate(translation);
        }
        protected override void SetVelocityX()
        {
            velocity.x = (targetWaypoint.x - transform.position.x) / timeToReachWaypoint;
        }
        protected override void SetVelocityY()
        {
            velocity.y = (targetWaypoint.y - transform.position.y) / timeToReachWaypoint;
        }

        void OnDrawGizmos()
        {
            var waypointRadius = 0.2f;
            if (PlatformWaypoints != null)
            {
                foreach (Vector2 wayPoint in PlatformWaypoints)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawWireSphere(new Vector3(wayPoint.x, wayPoint.y), waypointRadius);
                }
            }

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(gameObject.transform.position, waypointRadius);
        }
    }
}
﻿using UnityEngine;

namespace MetalProject.Enviroment
{
    public class PassangerMovment
    {
        public Transform passangerTransform { get; private set; }
        public Vector2 translation { get; private set; }
        public bool standingOnPlatform { get; private set; }
        public bool moveBeforePlatform { get; private set; }

        public PassangerMovment(Transform _transform, Vector2 _velocity, bool _standingOnPlatform, bool _moveBeforePlatform)
        {
            passangerTransform = _transform;
            translation = _velocity;
            standingOnPlatform = _standingOnPlatform;
            moveBeforePlatform = _moveBeforePlatform;
        }
    }
}

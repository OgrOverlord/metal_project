﻿using UnityEngine;
using MetalProject.CommonUtilty;

namespace MetalProject.Enviroment
{
    public class Platform : MonoBehaviour
    {
        [SerializeField] private MyEnums.PlatformType type;

        public MyEnums.PlatformType Type
        {
            get { return type; }
            private set { type = value; }
        }

    }
}
﻿using UnityEngine;
using MetalProject.Characters;

public class KillTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<ICharacter>() != null)
        {
            var character = collision.GetComponent<ICharacter>();
            character.Die();
        }
    }
}

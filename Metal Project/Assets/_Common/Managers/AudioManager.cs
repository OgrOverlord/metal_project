﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using MetalProject.CommonUtilty;
using MetalProject.CommonUtilty;

namespace MetalProject.Core
{
    public class AudioManager : MonoBehaviour
    {
        #region Serialized fields
        [Header("AudioMixer")]
        [SerializeField] private AudioMixer audioMixer;

        [Header("Snapshots")]
        [SerializeField] private float snapshotTransitionTime;

        [Header("SFX Distances for volum scale")]
        [SerializeField] [RangeAttribute(0, 20)] private float distanceFar;
        [SerializeField] [RangeAttribute(0,1)] private float volumeScaleFar;

        [SerializeField] [RangeAttribute(0, 20)] private float distanceMeddium;
        [SerializeField] [RangeAttribute(0, 1)] private float volumeScaleMedium;

        #endregion

        #region Fields
        private static AudioManager instance;

        [Header("AudioMixer Groups")]
        public AudioMixerGroup musicGroup; //TODO public field is a heresy but will have to do for now
        public AudioMixerGroup ambientGroup;
        public AudioMixerGroup sfxGroup;
        public AudioMixerGroup dialogGroup;

        private AudioSource musicSource;
        private AudioSource ambientSource;
        private AudioSource sfxSource;
        private AudioSource dialogSource;

        private bool firstMusiSourceIsPlaying;

        private GameManager gameManager;

        #endregion

        #region Properties
        public static AudioManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<AudioManager>();
                }

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        #endregion

        void Awake()
        {
            DontDestroyOnLoad(gameObject);
            SetupMusicSources();
        }
        void Start()
        {
            gameManager = FindObjectOfType<GameManager>();
            gameManager.OnGameStateChanged += ChangeMixerSnapshot;
        }

        private void SetupMusicSources()
        {
            musicSource = this.gameObject.AddComponent<AudioSource>();
            sfxSource = this.gameObject.AddComponent<AudioSource>();
            ambientSource = this.gameObject.AddComponent<AudioSource>();
            dialogSource = this.gameObject.AddComponent<AudioSource>();

            musicSource.loop = true;

            musicSource.outputAudioMixerGroup = musicGroup;
            ambientSource.outputAudioMixerGroup = ambientGroup;

            dialogSource.outputAudioMixerGroup = dialogGroup;

            sfxSource.outputAudioMixerGroup = sfxGroup;
        }
        private void ChangeMixerSnapshot(MyEnums.GameState state)
        {
            AudioMixerSnapshot targetSnapshot = audioMixer.FindSnapshot(state.ToString());
            targetSnapshot.TransitionTo(snapshotTransitionTime);
        }

        private IEnumerator UpdateMusicWithFade(AudioSource activeSource, AudioClip newClip, float transitionTime)
        {
            if (!activeSource.isPlaying)
            {
                activeSource.Play();
            }

            float transition = 0f;

            //Fade out
            for (transition = 0; transition < transitionTime; transition += Time.deltaTime)
            {
                activeSource.volume = (1 - (transition / transitionTime));
                yield return null;
            }

            activeSource.Stop();
            activeSource.clip = newClip;
            activeSource.Play();

            //Fade in
            for (transition = 0; transition < transitionTime; transition += Time.deltaTime)
            {
                activeSource.volume = (transition / transitionTime);
                yield return null;
            }

        }

        public void PlaySFX(AudioClip clip, float distance = 0f)
        {
            if (distance >= distanceFar) { sfxSource.PlayOneShot(clip, volumeScaleFar); }
            else if(distance >= distanceMeddium) { sfxSource.PlayOneShot(clip, volumeScaleMedium); }
            else { sfxSource.PlayOneShot(clip); }
        }
        public void PlaySFXAtVolume(AudioClip clip, float volumeScale = 1f)
        {
            sfxSource.PlayOneShot(clip, volumeScale);
        }
        public void PlayDialog(AudioClip clip, float distance = 0f)
        {
            if (distance >= distanceFar) { dialogSource.PlayOneShot(clip, volumeScaleFar); }
            else if (distance >= distanceMeddium) { dialogSource.PlayOneShot(clip, volumeScaleMedium); }
            else { dialogSource.PlayOneShot(clip); }
        }
        public void PlayWithFade(AudioClip newClip, MyEnums.SoundSourceType sourceType, float transitionTime = 1f)
        {
            if (sourceType == MyEnums.SoundSourceType.Music)
            {
                StartCoroutine(UpdateMusicWithFade(musicSource, newClip, transitionTime));
            }
            else if (sourceType == MyEnums.SoundSourceType.Ambient)
            {
                StartCoroutine(UpdateMusicWithFade(ambientSource, newClip, transitionTime));
            }
        }

    }
}
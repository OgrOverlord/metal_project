﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MetalProject.Characters;
using UnityEngine.SceneManagement;
using MetalProject.UI;
using MetalProject.CommonUtilty;
using MetalProject.Combat;
using UnityEngine.InputSystem;

namespace MetalProject.Core
{
    public class GameManager : MonoBehaviour
    {
        #region private fields

        private static GameManager gameManagerInstance;
        private InGameMenuController inGameMenuControler;
        private CrossfadeUI crossFadeUI;

        private CharacterSpawner[] characterSpawners;

        private string activeSceneName;
        private int activeSceneIndex;

        private string previousSceneName;
        private int previousSceneIndex = -1;

        #endregion

        #region Properties

        public InputMaster GameInputMaster { get; private set; }
        public PlayerInput MyPlayerInput { get; private set; }
        public MyEnums.GameState ActiveGameState { get; private set; }
        public int ActiveSceneIndex
        {
            get { return activeSceneIndex; }
            private set { activeSceneIndex = value; }
        }
        public bool ParticlesEnabled
        {
            get { return particlesEnalbed; }
            private set { particlesEnalbed = value; }
        }

        //TODO consider setting set to private
        public Dictionary<string, bool> CombatAreas { get; private set; } //Contains refernce to each combat area with a GUID key

        #endregion

        #region Serialzied Fields

        [Header("Gameplay")]
        [SerializeField] private float playerRespawnDelayTime;

        [Header("Settings")]
        [SerializeField] private bool particlesEnalbed;

        [Header("LevelData")]
        [SerializeField] private LevelData[] levelData;


        #endregion

        #region Events
        public delegate void OnGameStateChangedDelegate(MyEnums.GameState state);
        public event OnGameStateChangedDelegate OnGameStateChanged;


        #endregion

        void Awake()
        {
            if (gameManagerInstance != null && gameManagerInstance != this) { Destroy(this.gameObject); }
            else { gameManagerInstance = this; }

            DontDestroyOnLoad(gameObject);
            ResetPlayerData();
            SetupInputMaster();

            CombatAreas = new Dictionary<string, bool>();
        }
        void OnEnable()
        {
            GameInputMaster.UI.Enable();
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        #region public methods
        public void LoadScene(int sceneIndex)
        {
            gameManagerInstance.StartCoroutine(LoadWithDelay(sceneIndex)); //Corutine added to support fading effect
        }
        public void LoadScene(string sceneName)
        {
            gameManagerInstance.StartCoroutine(LoadWithDelay(sceneName)); //Corutine added to support fading effect
        }
        public void ExitToDesktop()
        {
            Application.Quit();
        }
        public void RespawnPlayer(Player player)
        {
            StartCoroutine(RespawnPlayerWithDelay(player));
        }
        public void UnpauseGame() //public method for UI in game resume button 
        {
            Time.timeScale = 1f;
            ActiveGameState = MyEnums.GameState.Game;
            gameManagerInstance.OnGameStateChanged(ActiveGameState);
            gameManagerInstance.GameInputMaster.Player.Enable();
            gameManagerInstance.inGameMenuControler.DisplayMenuShade(false);
            gameManagerInstance.inGameMenuControler.DisplayMenuButtons(false);
        }

        #endregion

        #region privite methods
        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            SetupInputMaster();
            GetActiveSceneData();
            SetInitialGameState();
            SetReferencToIngamemenu();
            SetReferencToCrossfadeUI();
            SpawnPlayer();
            SetLevelMusic();
            SetLevelBackground();
            DefineCombatAreaData();
        }

        private void DefineCombatAreaData()
        {
            CombatArea[] combatAreas = FindObjectsOfType<CombatArea>();

            if (combatAreas.Length != 0)
            {
                foreach (var area in combatAreas)
                {
                    if (!CombatAreas.ContainsKey(area.AreaID))
                    {
                        CombatAreas.Add(area.AreaID, false);
                    }
                }
            }

            //foreach (var item in CombatAreas) //Debug for dictionary
            //{
            //    Debug.Log(item.Key + " ------- " + item.Value);
            //}

        }

        private void OnSceneUnloaded(Scene arg0)
        {
            previousSceneIndex = ActiveSceneIndex;
            previousSceneName = activeSceneName;
        }

        private void ResetPlayerData()
        {
            PlayerData.Health = -1;

            PlayerData.NormalBullet = -1;
            PlayerData.PiercingBullets = -1;

            PlayerData.ExitAreaId = string.Empty;

            PlayerData.RespawnLevelIndex = -1;
            PlayerData.ActivatedShrinePositionX = 0f;
            PlayerData.ActivatedShrinePositionY = 0f;
            PlayerData.ShrineActivated = false;
            PlayerData.EnteredExitArea = false;

            PlayerData.PlayerDied = false;
        }
        private void GetActiveSceneData()
        {
            activeSceneName = SceneManager.GetActiveScene().name;

            for (int i = 0; i < levelData.Length; i++)
            {
                if (levelData[i].levelName == activeSceneName)
                {
                    activeSceneIndex = i;
                }
            }
        }
        private void SetInitialGameState()
        {
            ActiveGameState = levelData[ActiveSceneIndex].state;
        }
        private void SetLevelMusic()
        {
            if (ActiveGameState == MyEnums.GameState.Main_Menu && previousSceneIndex == -1) //Condtion needed for the start of the game
            {
                AudioManager.Instance.PlayWithFade(levelData[ActiveSceneIndex].levelSongs, MyEnums.SoundSourceType.Music);
            }
            else
            {
                if (previousSceneIndex != -1) //Condition needed for test scenes
                {
                    if (levelData[ActiveSceneIndex].levelSongs != levelData[previousSceneIndex].levelSongs)
                    {
                        AudioManager.Instance.PlayWithFade(levelData[ActiveSceneIndex].levelSongs, MyEnums.SoundSourceType.Music);
                    }
                    if (levelData[ActiveSceneIndex].levelAmbient != levelData[previousSceneIndex].levelAmbient)
                    {
                        AudioManager.Instance.PlayWithFade(levelData[ActiveSceneIndex].levelAmbient, MyEnums.SoundSourceType.Ambient);
                    }
                }
            }
        }
        private void SetLevelBackground()
        {
            if (levelData[ActiveSceneIndex].parallaxLevelBackground != null)
            {
                Instantiate(levelData[ActiveSceneIndex].parallaxLevelBackground);
            }
        }

        private void SetReferencToIngamemenu()
        {
            if (FindObjectOfType<InGameMenuController>() != null) { inGameMenuControler = FindObjectOfType<InGameMenuController>(); }
        }
        private void SetReferencToCrossfadeUI()
        {
            if (FindObjectOfType<CrossfadeUI>() != null) { crossFadeUI = FindObjectOfType<CrossfadeUI>(); }
        }

        private void SpawnPlayer()
        {
            characterSpawners = FindObjectsOfType<CharacterSpawner>();
            EntryPoint[] entryPoints = FindObjectsOfType<EntryPoint>();
            EntryPoint selectedEntryPoint = null;

            if (characterSpawners != null)
            {
                foreach (var spawner in characterSpawners)
                {
                    if (PlayerData.ShrineActivated && spawner.PlayerSpawner && PlayerData.PlayerDied && !PlayerData.EnteredExitArea) //set spawner position after player death
                    {
                        spawner.transform.position = new Vector3(PlayerData.ActivatedShrinePositionX, PlayerData.ActivatedShrinePositionY, 0);
                    }
                    else if (PlayerData.EnteredExitArea && spawner.PlayerSpawner) // set spawner position when moving between levels
                    {
                        foreach (var entryPoint in entryPoints)
                        {
                            if (entryPoint.Id == PlayerData.ExitAreaId)
                            {
                                spawner.transform.position = entryPoint.transform.position;
                                selectedEntryPoint = entryPoint;
                            }
                        }
                        PlayerData.EnteredExitArea = false;
                    }
                    if (spawner.PlayerSpawner)
                    {
                        GameObject player = spawner.SpawnCharacter();
                        if (selectedEntryPoint != null) { player.GetComponent<PlayerControler>().ChangePlayerLookingDirection(selectedEntryPoint.PlayerMovmentDirection); }
                    }
                    if (PlayerData.ShrineActivated && spawner.PlayerSpawner && PlayerData.RespawnLevelIndex == activeSceneIndex) //set spawner position to active shrine after spawning the player
                    {
                        spawner.transform.position = new Vector3(PlayerData.ActivatedShrinePositionX, PlayerData.ActivatedShrinePositionY, 0);
                    }
                }
            }
        }
        private void SetupInputMaster()
        {
            GameInputMaster = new InputMaster();
            GameInputMaster.UI.Pause.performed += ctx => PauseGame();
            MyPlayerInput = GetComponent<PlayerInput>();
        }

        private void PauseGame()
        {
            if (ActiveGameState == MyEnums.GameState.Game)
            {
                ActiveGameState = MyEnums.GameState.Paused_Menu;
                OnGameStateChanged(ActiveGameState);
                Time.timeScale = 0f;
                GameInputMaster.Player.Disable();
                inGameMenuControler.DisplayMenuShade(true);
                inGameMenuControler.DisplayMenuButtons(true);
            }
            else if (ActiveGameState == MyEnums.GameState.Paused_Menu)
            {
                UnpauseGame();
            }
        }


        IEnumerator RespawnPlayerWithDelay(Player player)
        {
            inGameMenuControler.DisplayMenuShade(true);
            inGameMenuControler.DisplayPlayerDefeatedText(true);
            GameInputMaster.Player.Disable();

            yield return new WaitForSeconds(playerRespawnDelayTime); //Delay the respawn of the player for the defined time

            if (PlayerData.ShrineActivated && PlayerData.RespawnLevelIndex != ActiveSceneIndex) //Player respawns on diffrent level
            {
                LoadScene(PlayerData.RespawnLevelIndex);
            }
            else //Player respawns on the same level 
            {
                player.ResetPlayerStatsOnRespawn();
                inGameMenuControler.DisplayMenuShade(false);
                inGameMenuControler.DisplayPlayerDefeatedText(false);

                foreach (var spawner in characterSpawners)
                {
                    if (spawner.PlayerSpawner) { player.gameObject.transform.position = spawner.transform.position; }
                }
                GameInputMaster.Player.Enable();
            }
        }

        IEnumerator LoadWithDelay(int sceneIndex)
        {
            Time.timeScale = 1f;
            if (gameManagerInstance.crossFadeUI)
            {
                gameManagerInstance.crossFadeUI.StartCrossfade();
                yield return new WaitForSeconds(gameManagerInstance.crossFadeUI.ScenaLoadDelay);
            }
            SceneManager.LoadScene(sceneIndex);
        }
        IEnumerator LoadWithDelay(string sceneName)
        {
            Time.timeScale = 1f;
            if (gameManagerInstance.crossFadeUI)
            {
                gameManagerInstance.crossFadeUI.StartCrossfade();
                yield return new WaitForSeconds(gameManagerInstance.crossFadeUI.ScenaLoadDelay);
            }
            SceneManager.LoadScene(sceneName);
        }
        #endregion




    }


}

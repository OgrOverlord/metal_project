﻿
namespace MetalProject.CommonUtilty
{
    public class MetalEventArgs //Class used to make sending paramters through events easier
    {
        #region Camera Shake fields
        public float ShakeDuration { get; private set; }
        public float ShakeMagnitude { get; private set; }
        public float DampingSpeed { get; private set; }

        #endregion


        public MetalEventArgs(float shakeDuration, float shakeMagnitude, float dampingSpeed)
        {
            this.ShakeDuration = shakeDuration;
            this.ShakeMagnitude = shakeMagnitude;
            this.DampingSpeed = dampingSpeed;
        }
    }
}
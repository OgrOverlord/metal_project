﻿using UnityEngine;
using MetalProject.CommonUtilty;
using NaughtyAttributes;

namespace MetalProject.Core
{
    [CreateAssetMenu(fileName = "New LevlData", menuName = "LevelData")]
    public class LevelData : ScriptableObject
    {
        public MyEnums.GameState state;
        public AudioClip levelSongs;
        public AudioClip levelAmbient;
        [ReadOnly] public string levelName;
        public string comment;

        [Tooltip("Background prefab for level parallax effect")]
        public GameObject parallaxLevelBackground;

        void OnEnable()
        {
            levelName = this.name;        
        }
    }

   
}
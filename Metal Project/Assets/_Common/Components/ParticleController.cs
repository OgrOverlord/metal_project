﻿using UnityEngine;
using MetalProject.Characters;

namespace MetalProject.Core
{
    public class ParticleController : MonoBehaviour
    {

        private GameManager gameManager;
        private Controler2D characterController;

        void Start()
        {
            gameManager = FindObjectOfType<GameManager>();
            characterController = GetComponent<Controler2D>();
        }

        public void SpawnParticle(Object myobject) //Note use only for particles that don't need proper rotation
        {
            if (gameManager.ParticlesEnabled && myobject != null)
            {
                GameObject gameObject = (GameObject)myobject; //TODO there probably is a more elegant way to do this
                ParticleSystem particel = gameObject.GetComponent<ParticleSystem>();
                Transform particelTransform = gameObject.GetComponent<Transform>();
                ParticleSystem instance = Instantiate(particel, transform.position + particelTransform.position, Quaternion.identity, transform);

            }
            else
            {
                Debug.LogError("Particle was not set");
            }
        }
        public void ActivateParticleSystem(string particleName)
        {
            ParticleSystem particleSystem = GameObject.Find(particleName).GetComponent<ParticleSystem>();
            particleSystem.transform.localScale = new Vector3(characterController.MovmentDirection, 1, 1);
            particleSystem.Play();

            if (particleSystem.transform.childCount > 0)
            {
                ParticleSystem[] childPartciles = particleSystem.GetComponentsInChildren<ParticleSystem>();
                foreach (var particle in childPartciles)
                {
                    particle.transform.localScale = new Vector3(characterController.MovmentDirection, 1, 1);
                    particle.Play();
                }
            }

        }



    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MetalProject.Enviroment;

namespace MetalProject.Core
{
    [RequireComponent(typeof(RaycastControler))]
    public class CollisionControler : MonoBehaviour
    {
        [SerializeField] LayerMask collisionMask;
        [SerializeField] [Range(0, 90)] float maxClimbAngle = 70f;
        [SerializeField] [Range(0, 90)] float maxDescendAngle = 70f;

        LayerMask passangerMask = 768; // player layer 8: bit 2^8 = 256; enemy layer 9: bit 2^9=512; 256+512 = 768
        RaycastControler raycastControler;
        float minimalRayLenght = 0.2f;

        public bool Above { get; private set; }
        public bool Below { get; private set; }
        public bool Left { get; private set; }
        public bool Right { get; private set; }
        public bool ClimbingSlope { get; private set; }
        public bool DescendingSlope { get; private set; }
        public float SlopeAngle { get; private set; }
        public float SlopeAngleOld { get; private set; }
        public void ResetCollisionInfoAndRaycastOrigins()
        {
            raycastControler.UpdateRaycastOriginPoints();

            Above = false;
            Below = false;
            Left = false;
            Right = false;
            ClimbingSlope = false;
            SlopeAngleOld = SlopeAngle;
            SlopeAngle = 0;
        }

        void Start()
        {
            raycastControler = GetComponent<RaycastControler>();
        }

        #region Characters Collisions
        public Vector2 VerticalCollisions(Vector2 translation) // Up and Down Collisions
        {
            float directionY = Mathf.Sign(translation.y);
            float rayLength;

            if (Mathf.Abs(translation.y) > 0.1) { rayLength = Mathf.Abs(translation.y) + RaycastControler.SKIN_WIDTH; }
            else { rayLength = minimalRayLenght; }

            for (int i = 0; i < raycastControler.VerticalRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? raycastControler.BottomLeft_OriginPoint : raycastControler.TopLeft_OriginPoint;
                rayOrigin += Vector2.right * raycastControler.VerticalRaySpacing * i;
                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

                if (hit)
                {
                    translation.y = (hit.distance - RaycastControler.SKIN_WIDTH) * directionY;
                    rayLength = hit.distance;

                    if (ClimbingSlope)
                    {
                        translation.x = Mathf.Abs(translation.y) / Mathf.Tan(SlopeAngle * Mathf.Deg2Rad);
                    }

                    Below = directionY == -1;
                    Above = directionY == 1;
                }

            }
            return translation;
        }
        public Vector2 HorizontalCollisions(Vector2 translation) // Left and Right Collisions 
        {
            float directionX = Mathf.Sign(translation.x);
            float rayLength; 

            if (Mathf.Abs(translation.x) > 0.1) { rayLength = Mathf.Abs(translation.x) + RaycastControler.SKIN_WIDTH; }
            else { rayLength = minimalRayLenght; }

            for (int i = 0; i < raycastControler.HorizontalRayCount; i++)
            {
                Vector2 rayOrigin = (directionX == -1) ? raycastControler.BottomLeft_OriginPoint : raycastControler.BottomRight_OriginPoint;
                rayOrigin += Vector2.up * raycastControler.HorizontalRaySpacing * i;
                Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

                if (hit)
                {
                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

                    if (slopeAngle != 0 && slopeAngle <= maxClimbAngle)
                    {
                        float distanceToSlopeStart = 0; //TODO Move to Method TranslateCharacterToSlope()
                        if (slopeAngle != SlopeAngleOld)
                        {
                            distanceToSlopeStart = hit.distance - RaycastControler.SKIN_WIDTH;
                            translation.x -= distanceToSlopeStart * directionX;
                        }
                        ClimbSlope(ref translation, slopeAngle);
                    }

                    if (!ClimbingSlope || slopeAngle > maxClimbAngle)
                    {
                        translation.x = (hit.distance - RaycastControler.SKIN_WIDTH) * directionX;
                        rayLength = hit.distance;

                        if (ClimbingSlope)  //TODO Move to Method AdjustTranslationForHorizontalCollisionOnSlope() 
                        {
                            translation.y = Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(translation.x);
                        }

                        Left = directionX == -1;
                        Right = directionX == 1;
                    }
                }
            }
            return translation;
        }
        public void ClimbSlope(ref Vector2 translation, float slopeAngle) //TODO consdier how to make it no refrence
        {
            float moveDistance = Mathf.Abs(translation.x);
            float climbTranslationY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

            if (translation.y <= climbTranslationY)
            {
                translation.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(translation.x);
                translation.y = climbTranslationY;

                ClimbingSlope = true;
                Below = true;
                this.SlopeAngle = slopeAngle;
            }
        }
        public Vector2 DescendSlope(Vector2 translation)
        {
            float directionX = Mathf.Sign(translation.x);
            float rayLength = 1f;

            Vector2 rayOrigin = (directionX == -1) ? raycastControler.BottomRight_OriginPoint : raycastControler.BottomLeft_OriginPoint;
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.down, rayLength, collisionMask);

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

                if (slopeAngle != 0 && slopeAngle <= maxDescendAngle)
                {
                    if (Mathf.Sign(hit.normal.x) == directionX)
                    {
                        if (hit.distance - RaycastControler.SKIN_WIDTH <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Math.Abs(translation.x))
                        {
                            float moveDistance = Mathf.Abs(translation.x);
                            float descendTranslationY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                            translation.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(translation.x);
                            translation.y -= descendTranslationY;

                            this.SlopeAngle = slopeAngle;
                            Below = true;
                            DescendingSlope = true;
                        }
                    }
                }
            }
            return translation;
        }
        public void StandingOnPlatform()
        {
            Below = true;
        }
        #endregion

        #region Platform Passangers Collisions
        public List<PassangerMovment> MovePassangerDownWithPlatform(Vector2 translation, HashSet<Transform> movedPassangers, float directionY, List<PassangerMovment> passangerMovments)
        {
            float rayLength = 2 * RaycastControler.SKIN_WIDTH;

            for (int i = 0; i < raycastControler.VerticalRayCount; i++)
            {
                Vector2 rayOrigin = raycastControler.TopLeft_OriginPoint; //allways cast from top for player standing on top
                rayOrigin += Vector2.right * raycastControler.VerticalRaySpacing * i;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, passangerMask);
                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.blue);

                if (hit)
                {
                    if (!movedPassangers.Contains(hit.transform))
                    {
                        movedPassangers.Add(hit.transform);
                        float pushX = translation.x;
                        float pushY = translation.y;

                        passangerMovments.Add(new PassangerMovment(hit.transform, new Vector2(pushX, pushY), true, false));
                    }
                }
            }
            return passangerMovments;
        }
        public List<PassangerMovment> PushPassangerToSides(Vector2 translation, HashSet<Transform> movedPassangers, float directionX, float directionY, List<PassangerMovment> passangerMovments)
        {
            float rayLength = Mathf.Abs(translation.x) + RaycastControler.SKIN_WIDTH;

            for (int i = 0; i < raycastControler.HorizontalRayCount; i++)
            {
                Vector2 rayOrigin = (directionX == -1) ? raycastControler.BottomLeft_OriginPoint : raycastControler.BottomRight_OriginPoint;
                rayOrigin += Vector2.up * raycastControler.HorizontalRaySpacing * i;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, passangerMask);
                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.blue);


                if (hit)
                {
                    if (!movedPassangers.Contains(hit.transform))
                    {
                        movedPassangers.Add(hit.transform);
                        float pushY = -RaycastControler.SKIN_WIDTH;
                        float pushX = translation.x - (hit.distance - RaycastControler.SKIN_WIDTH) * directionX;

                        passangerMovments.Add(new PassangerMovment(hit.transform, new Vector2(pushX, pushY), false, true));

                    }
                }
            }
            return passangerMovments;
        }
        public List<PassangerMovment> PushPasangerUp(Vector2 translation, HashSet<Transform> movedPassangers, float directionY, List<PassangerMovment> passangerMovments)
        {
            float rayLength = Mathf.Abs(translation.y) + RaycastControler.SKIN_WIDTH;

            for (int i = 0; i < raycastControler.VerticalRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? raycastControler.BottomLeft_OriginPoint : raycastControler.TopLeft_OriginPoint;
                rayOrigin += Vector2.right * raycastControler.VerticalRaySpacing * i;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passangerMask);
                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.green);

                if (hit)
                {
                    if (!movedPassangers.Contains(hit.transform))
                    {
                        movedPassangers.Add(hit.transform);
                        float pushX = (directionY == 1) ? translation.x : 0;
                        float pushY = translation.y - (hit.distance - RaycastControler.SKIN_WIDTH) * directionY;

                        passangerMovments.Add(new PassangerMovment(hit.transform, new Vector2(pushX, pushY), directionY == 1, true)); //Add passanger to the list with value of its translation
                    }
                }
            }
            return passangerMovments;
        }
        #endregion


        #region Public Methods
        public int GetCollisionMask()
        {
            return collisionMask;
        }

        #endregion


    }

}
﻿using UnityEngine;
using MetalProject.Core;

namespace MetalProject.Characters
{
    [RequireComponent(typeof(CollisionControler))]
    public abstract class Controler2D : MonoBehaviour
    {
        protected Vector2 translation;
        protected CollisionControler collisionControler;

        public float MovmentDirection { get; protected set; }

        protected void Start()
        {
            collisionControler = GetComponent<CollisionControler>();
        }
        public virtual void Move(Vector2 translation, bool standingOnPlatform = false) //TODO think of the way to make it use only translation parameter
        {
            collisionControler.ResetCollisionInfoAndRaycastOrigins();
            this.translation = translation;

            if (translation.y < 0)
            {
                this.translation = collisionControler.DescendSlope(translation); 
                translation = this.translation;
            }

            if (translation.x != 0)
            {
                this.translation = collisionControler.HorizontalCollisions(translation);
                translation = this.translation;
            }
            if (translation.y != 0)
            {
                this.translation = collisionControler.VerticalCollisions(translation);
                translation = this.translation;
            }

            transform.Translate(translation);

            if (standingOnPlatform)
            {
                collisionControler.StandingOnPlatform();
            }
        }

        protected abstract void SetVelocityX();
        protected abstract void SetVelocityY();
    
    }
}
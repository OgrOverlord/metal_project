﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MetalProject.Core
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class RaycastControler : MonoBehaviour
    {
        [SerializeField] [Range(2, 15)] int horizontalRayCount = 4;
        [SerializeField] [Range(2, 15)] int verticalRayCount = 4;

        public int HorizontalRayCount
        {
            get { return horizontalRayCount; }
            private set { horizontalRayCount = value; }
        }
        public int VerticalRayCount
        {
            get { return verticalRayCount; }
            private set { verticalRayCount = value; }
        }
        public float HorizontalRaySpacing { get; private set; }
        public float VerticalRaySpacing { get; private set; }
        public Vector2 BottomLeft_OriginPoint { get; private set; }
        public Vector2 BottomRight_OriginPoint { get; private set; }
        public Vector2 TopLeft_OriginPoint { get; private set; }
        public Vector2 TopRight_OriginPoint { get; private set; }
        public Vector2 Center_OriginPoint { get; private set; }

        public const float SKIN_WIDTH = 0.03125f; //Value of one pixel in Unity Scale, Pixels per unit = 32

        Collider2D characterCollider;

        void Start()
        {
            characterCollider = GetComponent<Collider2D>();
            CalculateRaySpacing();
        }

        public void UpdateRaycastOriginPoints()
        {
            Bounds bounds = characterCollider.bounds;
            bounds.Expand(SKIN_WIDTH * -2);

            BottomLeft_OriginPoint = new Vector2(bounds.min.x, bounds.min.y);
            BottomRight_OriginPoint = new Vector2(bounds.max.x, bounds.min.y);

            TopLeft_OriginPoint = new Vector2(bounds.min.x, bounds.max.y);
            TopRight_OriginPoint = new Vector2(bounds.max.x, bounds.max.y);

            Center_OriginPoint = new Vector2(bounds.center.x, bounds.center.y);
        }
        public void CalculateRaySpacing()
        {
            Bounds bounds = characterCollider.bounds;
            bounds.Expand(SKIN_WIDTH * -2);

            HorizontalRaySpacing = bounds.size.y / (HorizontalRayCount - 1);
            VerticalRaySpacing = bounds.size.x / (VerticalRayCount - 1);
        }
    }
}
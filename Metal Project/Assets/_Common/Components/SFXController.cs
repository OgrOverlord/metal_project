﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.CommonUtilty;
using MetalProject.Enviroment;
using NaughtyAttributes;
using System.Collections;

namespace MetalProject.Core
{
    public class SFXController : MonoBehaviour
    {
        [Header("Footsteps")]
        [SerializeField] [ShowIf("ControllerOnCharacter")] private LayerMask footstepCollisionMask;
        [SerializeField] [ShowIf("ControllerOnCharacter")] private AudioClip[] footstepSoundsStone;
        [SerializeField] [ShowIf("ControllerOnCharacter")] private AudioClip[] footstepSoundsWood;

        [Header("Getting Hit")]
        [SerializeField] [ShowIf("ControllerOnCharacter")] private AudioClip[] GettingHitSFX;
        [SerializeField] [ShowIf("ControllerOnCharacter")] private AudioClip[] GettingHitScreams;

        private MyEnums.PlatformType activePlatformType;

        private AudioManager audioManager;
        private PlayerHearingArea playerHearingArea;
        private ICharacter character;
        private AudioSource localAudioSource;

        void Start()
        {
            audioManager = FindObjectOfType<AudioManager>();
            playerHearingArea = FindObjectOfType<PlayerHearingArea>();
            localAudioSource = GetComponent<AudioSource>();
            character = GetComponent<ICharacter>();
        }

        void Update()
        {
            if (character != null) { CheckPlatformType(); }
            if (localAudioSource != null) //Used for game objects that have their own audiosoruce for playing looping audio effects
            {
                bool soundSourceWithinRange = playerHearingArea.CheckIfSoundSourceIsInRange(gameObject.transform.position);
                if (soundSourceWithinRange) { SetLocalAudioSoruceVolume(Vector3.Distance(playerHearingArea.transform.position, gameObject.transform.position)); }
                if (localAudioSource.loop && !localAudioSource.isPlaying && soundSourceWithinRange) { localAudioSource.Play(); }
                if (localAudioSource.isPlaying && !soundSourceWithinRange) { localAudioSource.Stop(); }
            }
        }
        #region Method rasied by animation events
        public void PlayEffect(Object myobject)
        {
            if (playerHearingArea.CheckIfSoundSourceIsInRange(this.gameObject.transform.position))
            {
                float distance = Vector3.Distance(this.gameObject.transform.position, playerHearingArea.transform.position);
                AudioClip audioClip = myobject as AudioClip;
                audioManager.PlaySFX(audioClip, distance);
            }
        }
        public void PlayFootStepSound()
        {
            if (playerHearingArea.CheckIfSoundSourceIsInRange(this.gameObject.transform.position))
            {
                float distance = Vector3.Distance(this.gameObject.transform.position, playerHearingArea.transform.position);
                if (activePlatformType == MyEnums.PlatformType.Stone)
                {
                    audioManager.PlaySFX(footstepSoundsStone[Random.Range(0, footstepSoundsStone.Length)], distance);
                }
                else if (activePlatformType == MyEnums.PlatformType.Wood)
                {
                    audioManager.PlaySFX(footstepSoundsWood[Random.Range(0, footstepSoundsWood.Length)], distance);
                }
            }
        }
        public void PlayRandomGetHitSound()
        {
            if (playerHearingArea.CheckIfSoundSourceIsInRange(this.gameObject.transform.position))
            {
                if (GettingHitSFX.Length != 0) { audioManager.PlaySFX(GettingHitSFX[Random.Range(0, GettingHitSFX.Length - 1)]); }
                if (GettingHitScreams.Length != 0) { audioManager.PlayDialog(GettingHitScreams[Random.Range(0, GettingHitScreams.Length - 1)]); }
            }
        }

        #endregion

        private void CheckPlatformType()
        {
            RaycastHit2D hit = Physics2D.Raycast(this.transform.position, Vector2.down, 2f, footstepCollisionMask);
            if (hit && hit.collider.gameObject.GetComponent<Platform>() != null)
            {
                activePlatformType = hit.collider.gameObject.GetComponent<Platform>().Type;
            }
        }
        private void SetLocalAudioSoruceVolume(float distance)
        {
            if (distance != 0) { localAudioSource.volume = 4 / distance; }
            else { localAudioSource.volume = 1f; }
        }

        private bool ControllerOnCharacter() //Used by Naughty Attributues to hide unnecesarry fields 
        {
            character = GetComponent<ICharacter>();
            if (character != null) { return true; }
            else { return false; }
        }


    }

}
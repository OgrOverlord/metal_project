﻿using UnityEngine;
using MetalProject.CommonUtilty;
using System;

namespace MetalProject.Enviroment
{
    public class ParallaxController : MonoBehaviour
    {
        [SerializeField] private Vector2 parralaxEffectMultiplier;
        [SerializeField] private bool infiniteHorizontal;
        [SerializeField] private bool infiniteVertical;


        private Transform cameraTransform;
        private Vector3 lastCameraPosition;

        private SpriteRenderer spriteRenderer;
        private float textureUnitSizeX;
        private float textureUnitSizeY;

        private bool initialSetupDone = false;


        void Start()
        {
            cameraTransform = Camera.main.transform;
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();

            Sprite sprite = spriteRenderer.sprite;
            Texture2D texture = sprite.texture;
            textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
            textureUnitSizeY = texture.height / sprite.pixelsPerUnit;
        }

        void Update()
        {
            InitialSetup();
        }

        void LateUpdate()
        {
            MoveTextureWithParallaxMultipler();
            if (infiniteHorizontal) { MoveTextureWhenLenghtExceded(); }
            if (infiniteVertical) { MoveTextureWhenHeightExceded(); }
        }

        private void InitialSetup()
        {
            if (!initialSetupDone)
            {
                transform.position = new Vector3(cameraTransform.position.x, cameraTransform.position.y, 10f);
                initialSetupDone = true;
                lastCameraPosition = cameraTransform.position;
            }
        }
        private void MoveTextureWithParallaxMultipler()
        {
            Vector3 deltaMovment = cameraTransform.position - lastCameraPosition;
            lastCameraPosition = cameraTransform.position;
            transform.position += new Vector3(deltaMovment.x * parralaxEffectMultiplier.x, deltaMovment.y * parralaxEffectMultiplier.y);
        }
        private void MoveTextureWhenHeightExceded()
        {
            if (Mathf.Abs(cameraTransform.position.y - transform.position.y) >= textureUnitSizeY)
            {
                float offsetPositonY = (cameraTransform.position.y - transform.position.y) % textureUnitSizeY;
                transform.position = new Vector3(cameraTransform.position.x, transform.position.y + offsetPositonY);
            }
        }
        private void MoveTextureWhenLenghtExceded()
        {
            if (Mathf.Abs(cameraTransform.position.x - transform.position.x) >= textureUnitSizeX)
            {
                float offsetPositonX = (cameraTransform.position.x - transform.position.x) % textureUnitSizeX;
                transform.position = new Vector3(cameraTransform.position.x + offsetPositonX, transform.position.y);
            }
        }
    }
}

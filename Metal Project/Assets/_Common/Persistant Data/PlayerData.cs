﻿
namespace MetalProject.Characters
{
    public static class PlayerData
    {
        public static int Health { get; set; }

        public static int NormalBullet { get; set; }
        public static int PiercingBullets { get; set; }

        public static string ExitAreaId { get; set; }

        public static int RespawnLevelIndex { get; set; }
        public static float ActivatedShrinePositionX { get; set; }
        public static float ActivatedShrinePositionY { get; set; }
        public static bool ShrineActivated { get; set; }
        public static bool EnteredExitArea { get; set; }

        public static bool PlayerDied { get; set; }

        static PlayerData()
        {
            Health = -1;

            NormalBullet = -1;
            PiercingBullets = -1;

            ExitAreaId = string.Empty;

            RespawnLevelIndex = -1;
            ActivatedShrinePositionX = 0f;
            ActivatedShrinePositionY = 0f;
            ShrineActivated = false;
            EnteredExitArea = false;

            PlayerDied = false;
        }
    }
}
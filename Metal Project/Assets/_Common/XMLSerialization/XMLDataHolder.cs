﻿namespace MetalProject.Core
{
    public static class XMLDataHolder //Class used to have an easy way to change paths to the xml files in all places they are referenced
    {
        public static string PlayerXMLDataPath { get; set;}

        static XMLDataHolder()
        {
            PlayerXMLDataPath = "PlayerData.xml";
        }
    }
}
﻿namespace MetalProject.CommonUtilty
{
    public static class MyEnums
    {
        public enum EnemyAttack //Enemy attacks for animation graph
        {
            NormalAttack = 1,
            FireDashAttack = 2,
            Fireball = 3,
            FireBlast = 4,
            JumpAttack = 5,
        }

        public enum BossAttackType //Types of boss character attacks
        {
            SmashAttack = 1,
            StompAttack = 2,
            FireStreamAttack = 3
        }

        public enum GameState //State of the game used by the GameManager.cs
        {
            Intro,
            Main_Menu,
            Game,
            Paused_Menu
        }

        public enum SoundSourceType //Used to defined which music source should be used
        {
            Music,
            Ambient
        }


        public enum PlatformType
        {
            Stone,
            Wood
        }

        public enum CombatConditionType //Combat Area condtions to spawn enemies
        {
            EnterArenaCondtion = 0,
            TimeCondtion = 1,
            KillAllActiveEnemiesCondition = 2,
            BossAtHalfHealth = 3
        }
    }
}
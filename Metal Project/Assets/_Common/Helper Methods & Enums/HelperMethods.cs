﻿using UnityEngine;
using System.IO;

namespace MetalProject.CommonUtilty
{
    public static class HelperMethods
    {
        public static int LayerMaskToLayer(LayerMask layerMask) //TODO probably this one can be completly replaced with 2 other methods
        {
            int layerNumber = 0;
            int layer = layerMask.value;
            while (layer > 0)
            {
                layer = layer >> 1;
                layerNumber++;
            }
            return layerNumber - 1;
        }
        public static bool CheckIfLayerIsInLayerMask(int layer, LayerMask includedLayerMask)
        {
            if (((1 << layer) & includedLayerMask) != 0) { return true; }
            else { return false; }
        }
        public static bool CheckIfLayerIsNOTInLayerMask(int layer, LayerMask ignoreLayerMask)
        {
            if (((1 << layer) & ignoreLayerMask) == 0) { return true; }
            else { return false; }
        }

        public static bool CheckIfFileExists(string path)
        {
            if (File.Exists(path)) { return true; }
            else { return false; }
        }
        public static void DeleteFile(string path)
        {
            if (File.Exists(path)) { File.Delete(path); }
        }
    }
}
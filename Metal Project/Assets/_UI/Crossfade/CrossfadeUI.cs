﻿using UnityEngine;

namespace MetalProject.UI
{
    public class CrossfadeUI : MonoBehaviour
    {
        [SerializeField] private string crossfadeTrigger;
        [SerializeField] private float scenaLoadDelay;
        private Animator animator;

        public float ScenaLoadDelay
        {
            get { return scenaLoadDelay; }
        }

        void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void StartCrossfade()
        {
            animator.SetTrigger(crossfadeTrigger);
        }
    }
}
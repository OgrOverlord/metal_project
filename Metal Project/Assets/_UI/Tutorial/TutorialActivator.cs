﻿using MetalProject.CommonUtilty;
using UnityEngine;
using TMPro;
using NaughtyAttributes;


namespace MetalProject.UI
{
    public class TutorialActivator : MonoBehaviour
    {
        [SerializeField] [ResizableTextArea] string tutorialText;
        [SerializeField] LayerMask includeLayerMask;

        TextMeshProUGUI tutorialTextUI;

        void Awake()
        {
            tutorialTextUI = FindObjectOfType<TutorialTextUI>().GetComponent<TextMeshProUGUI>();
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (HelperMethods.CheckIfLayerIsInLayerMask(collision.gameObject.layer, includeLayerMask))
            {
                tutorialTextUI.text = tutorialText;
                tutorialTextUI.color = Color.white;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (HelperMethods.CheckIfLayerIsInLayerMask(collision.gameObject.layer, includeLayerMask))
            {
                tutorialTextUI.color = Color.clear;
            }

        }
    }
}

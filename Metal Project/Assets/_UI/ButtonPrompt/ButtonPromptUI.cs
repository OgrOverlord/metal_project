﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using MetalProject.Core;
using TMPro;

namespace MetalProject.UI
{
    public class ButtonPromptUI : MonoBehaviour
    {
        [SerializeField] private GameObject KeyboardBackground;
        [SerializeField] private GameObject KeyboardText;

        [SerializeField] private GameObject ConsoleButton;

        private GameManager gameManager;
        private string activeControlSchem;

        void Start()
        {
            TextMeshProUGUI text;

            gameManager = FindObjectOfType<GameManager>();
            ChangeControlsPrompts(gameManager.MyPlayerInput.currentControlScheme);
            text = KeyboardText.GetComponent<TextMeshProUGUI>();
            text.SetText(gameManager.GameInputMaster.Player.Interaction.controls[0].displayName);
        }
        void Update()
        {
            if (gameManager.MyPlayerInput.currentControlScheme != activeControlSchem)
            {
                activeControlSchem = gameManager.MyPlayerInput.currentControlScheme;
                ChangeControlsPrompts(gameManager.MyPlayerInput.currentControlScheme);
            }
        }

        private void ChangeControlsPrompts(string controlSchemeName)
        {
            if (controlSchemeName == "Keyboard and mouse")
            {
                KeyboardBackground.SetActive(true);
                KeyboardText.SetActive(true);

                ConsoleButton.SetActive(false);
            }
            else if (controlSchemeName == "Controllers")
            {
                KeyboardBackground.SetActive(false);
                KeyboardText.SetActive(false);

                ConsoleButton.SetActive(true);
            }
        }
    }

}
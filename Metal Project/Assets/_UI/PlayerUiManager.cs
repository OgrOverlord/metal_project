﻿using MetalProject.Characters;
using MetalProject.Combat;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MetalProject.UI
{
    public class PlayerUiManager : MonoBehaviour
    {

        Player player;
        PlayerCombatControler playerCombatControler;
        PlayerAnimationManager playerAnimationManager;

        Slider healthBar;
        Slider staminaBar;

        TextMeshProUGUI numberOfBullets;
        Image bulletImage;

        void Start()
        {
            player = FindObjectOfType<Player>();
            playerCombatControler = FindObjectOfType<PlayerCombatControler>();

            staminaBar = FindObjectOfType<StaminaBarUI>().GetComponent<Slider>();
            healthBar = FindObjectOfType<HealthBarUI>().GetComponent<Slider>();
            numberOfBullets = FindObjectOfType<NumberOfBulletsUI>().GetComponent<TextMeshProUGUI>();
            bulletImage = FindObjectOfType<BulletImageUI>().GetComponent<Image>();

            healthBar.maxValue = player.MaxHealth;
            staminaBar.maxValue = player.MaxStamina;
            SwitchBulletImage(0);

            playerCombatControler.OnSwitchBulletType += SwitchBulletImage;
            playerCombatControler.OnNumberOFBulletsReduced += UpdateNumberOfBulletsText;
        }

        void Update()
        {
            UpdatePlayerStaminaBar();
            UpdatePlayerHealthBar();
        }

        private void UpdatePlayerHealthBar()
        {
            healthBar.value = player.CurentHealth;
        }
        private void UpdatePlayerStaminaBar()
        {
            staminaBar.value = player.CurentStamina;
        }

        private void SwitchBulletImage(int activeBulletIndex)
        {
            bulletImage.sprite = playerCombatControler.ActiveBulletType.bulletSprite;
            UpdateNumberOfBulletsText(playerCombatControler.AvailableBullets[activeBulletIndex], true);
        }
        private void UpdateNumberOfBulletsText(int value, bool updateBulletText)
        {
            if (updateBulletText) { numberOfBullets.text = "x " + value; }
        }




    }
}

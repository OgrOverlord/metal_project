﻿using UnityEngine;
using MetalProject.Characters;
using UnityEngine.UI;
using System;

namespace MetalProject.UI
{
    public class EnemyHealthBarUI : MonoBehaviour
    {
        Enemy enemy;
        Slider healthBar;
        Canvas EnemyUI;

        bool enemyDied;

        void Start()
        {
            enemy = GetComponentInParent<Enemy>();
            healthBar = GetComponent<Slider>();
            EnemyUI = GetComponentInParent<Canvas>();

            healthBar.maxValue = enemy.MaxHealth;
            healthBar.value = enemy.CurentHealth;
            enemy.OnEnemyDies += HideHealthbar;
            enemy.OnEnemyHealthModifed += UpdateHealthBar;

            EnemyUI.enabled = false;
        }
        void Update()
        {
            this.gameObject.transform.localScale = new Vector3(-enemy.gameObject.transform.localScale.x, 1f, 1f);
        }

        private void HideHealthbar(Enemy enemy)
        {
            EnemyUI.enabled = false;
            if (enemy.CurentHealth <= 0) { enemyDied = true; }
        }

        private void UpdateHealthBar(int value)
        {
            healthBar.value += value;
            EnemyUI.enabled = true;
        }
    }
}
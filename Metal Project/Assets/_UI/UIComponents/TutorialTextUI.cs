﻿using TMPro;
using UnityEngine;

namespace MetalProject.UI
{
    public class TutorialTextUI : MonoBehaviour
    {
        TextMeshProUGUI text;

        private void Start()
        {
            text = GetComponent<TextMeshProUGUI>();
            text.color = Color.clear;           
        }
    }
}
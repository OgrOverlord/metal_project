﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetalProject.UI
{
    public class InGameMenuController : MonoBehaviour
    {
        private PasueMenuShadeUI menuShade;
        private InGameMenuButtonsUI menuButtons;
        private PlayerDefeatedUI playerDefeatedText;

        private bool includeInactive = true;

        private void Start()
        {
            menuShade = GetComponentInChildren<PasueMenuShadeUI>(includeInactive);
            menuButtons = GetComponentInChildren<InGameMenuButtonsUI>(includeInactive);
            playerDefeatedText = GetComponentInChildren<PlayerDefeatedUI>(includeInactive);
        }

        public void DisplayMenuShade(bool display)
        {
            menuShade.gameObject.SetActive(display);
        }
        public void DisplayMenuButtons(bool display)
        {
            menuButtons.gameObject.SetActive(display);
        }
        public void DisplayPlayerDefeatedText(bool display)
        {
            playerDefeatedText.gameObject.SetActive(display);
        }
    }
}
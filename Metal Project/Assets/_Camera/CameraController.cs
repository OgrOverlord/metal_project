﻿using System.Collections;
using UnityEngine;
using MetalProject.Characters;
using MetalProject.CommonUtilty;

namespace MetalProject.Core
{
    public class CameraController : MonoBehaviour
    {
        [Header("Camera size and offsets")]
        [SerializeField] float sprintCameraSize;
        [SerializeField] float runCameraSize;
        [SerializeField] float cameraOfsetRightAndTop;
        [SerializeField] float cameraOfsetLeftAndDown;

        [Header("Other")]
        [SerializeField] float killTriggerOffset;

        PlayerControler playerControler;
        PlayerAnimationManager playerAnimationManager;
        Player player;
        Camera myCamera;

        Vector3 cameraPosition;
        Vector2 cameraOfset;

        float cameraStopFollowYValue;

        float shakeDuration;
        float shakeMagnitude;
        float dampingSpeed;

        float cameraSize;

        void Start()
        {
            myCamera = GetComponent<Camera>();
            playerControler = FindObjectOfType<PlayerControler>();
            player = playerControler.gameObject.GetComponent<Player>();
            playerAnimationManager = playerControler.gameObject.GetComponent<PlayerAnimationManager>();

            transform.position = new Vector3(playerControler.transform.position.x, playerControler.transform.position.y, transform.position.z);

            try
            {
                KillTrigger killTrigger = FindObjectOfType<KillTrigger>();
                cameraStopFollowYValue = killTrigger.transform.position.y + killTriggerOffset;
            }
            catch { throw new MissingReferenceException("Kill trigger not found on the level or was disabled!"); }

            playerControler.OnPlayerHardCollide += SetShakeParameters;
            playerControler.OnPlayerSprintingOrRunning += SetCameraSize;
            player.OnPlayerGettingHit += SetShakeParameters;
            playerAnimationManager.OnPlayerChargedAttack += SetShakeParameters;

            myCamera.orthographicSize = runCameraSize;
            cameraOfset.y = 3f;
        }
        void Update()
        {
            if (shakeDuration > 0) { ShakeCamera(); }
            else if (playerControler.transform.position.y > cameraStopFollowYValue)
            {
                shakeDuration = 0f;
                transform.position = new Vector3(playerControler.transform.position.x + cameraOfset.x, playerControler.transform.position.y + cameraOfset.y, transform.position.z);
            }

            ChangeLookDirection();
        }

        public void SetShakeParameters(float shakeDuration, float shakeMagnitude, float dampingSpeed) //Option needed for boss. Camera shoudn't be resposible for checking if boss was spwned
        {
            this.shakeDuration = shakeDuration;
            this.shakeMagnitude = shakeMagnitude;
            this.dampingSpeed = dampingSpeed;
        }

        private void SetCameraSize(bool sprinting)
        {
            if (sprinting)
            {
                cameraSize = sprintCameraSize;
                StartCoroutine(IncreseCameraSize());
            }
            else
            {
                cameraSize = runCameraSize;
                StartCoroutine(DesreseCameraSize());
            }
        }
        private void ShakeCamera()
        {
            transform.position = new Vector3(playerControler.transform.position.x + cameraOfset.x,
                                            playerControler.transform.position.y + cameraOfset.y,
                                            transform.position.z) + Random.insideUnitSphere * shakeMagnitude;
            shakeDuration -= Time.deltaTime * dampingSpeed;
        }
        private void SetShakeParameters(MetalEventArgs args)
        {
            this.shakeDuration = args.ShakeDuration;
            this.shakeMagnitude = args.ShakeMagnitude;
            this.dampingSpeed = args.DampingSpeed;
        }

        private void ChangeLookDirection()
        {
            if (playerControler.MovmentDirection == Vector2.right.x)
            {
                StartCoroutine(SlowlyLookRight(cameraOfsetRightAndTop));
            }

            if (playerControler.MovmentDirection == Vector2.left.x && cameraOfset.x > 0)
            {
                StartCoroutine(SlowlyLookLeft(cameraOfsetLeftAndDown));
            }

        }

        IEnumerator SlowlyLookRight(float targetOffset)
        {
            float step = 0.05f;

            while (cameraOfset.x <= targetOffset)
            {
                cameraOfset.x += step;
                yield return null;
            }

        }
        IEnumerator SlowlyLookLeft(float targetOffset)
        {
            float step = 0.05f;

            while (cameraOfset.x >= targetOffset)
            {
                cameraOfset.x -= step;
                yield return null;
            }

        }

        IEnumerator IncreseCameraSize()
        {
            float step = 0.25f;

            while (cameraSize > myCamera.orthographicSize)
            {
                myCamera.orthographicSize += step;
                yield return null;
            }
        }
        IEnumerator DesreseCameraSize()
        {
            float step = 0.25f;

            while (cameraSize < myCamera.orthographicSize)
            {
                myCamera.orthographicSize -= step;
                yield return null;
            }
        }


    }
}



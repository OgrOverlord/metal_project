﻿using MetalProject.Characters;
using UnityEngine;
using System.Collections.Generic;
using MetalProject.CommonUtilty;
using MetalProject.Core;
using UnityEngine.Experimental.Rendering.Universal;
using System;

namespace MetalProject.Combat
{
    public class CombatArea : MonoBehaviour
    {
        #region Serialzied fields
        [SerializeField] List<CombatScenario> scenarios;
        [SerializeField] List<GameObject> blockers;

        [SerializeField] string areaID;

        #endregion

        #region Event and delegates
        public delegate void OnPlayerEnteredDelegate(bool value);
        public event OnPlayerEnteredDelegate OnPlayerEntered;

        #endregion

        #region Fields
        private bool battleStarted;
        private float battleStartedTime;
        private BattleStartTrigger battleStartTrigger;

        private int numberOfLivingEnemies;

        private GameManager gameManager;

        private Boss bossReference;
        #endregion

        #region Properties
        public string AreaID
        {
            get { return areaID; }
            private set { areaID = value; }
        }

        #endregion

        void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();

            numberOfLivingEnemies = 0;
            battleStarted = false;

            battleStartTrigger = GetComponentInChildren<BattleStartTrigger>();
            battleStartTrigger.OnPlayerTriggersBattle += StartBattle;
        }

        void Update()
        {
            if (!gameManager.CombatAreas[AreaID])
            {
                if (battleStarted && scenarios.Count != 0)
                {
                    for (int i = (scenarios.Count - 1); i >= 0; i--)
                    {
                        if (scenarios.Count != 0 && scenarios[i].Condition == MyEnums.CombatConditionType.EnterArenaCondtion && !scenarios[i].ScenarioTriggered)
                        {
                            EvaluateEnteredAreaCondtion(i);
                            break;
                        }
                        if (scenarios.Count != 0 && scenarios[i].Condition == MyEnums.CombatConditionType.TimeCondtion && !scenarios[i].ScenarioTriggered)
                        {
                            EvaluateTimeCondtion(i);
                            break;
                        }
                        if (scenarios.Count != 0 && scenarios[i].Condition == MyEnums.CombatConditionType.KillAllActiveEnemiesCondition && !scenarios[i].ScenarioTriggered)
                        {
                            EvaluateKillAllActiveEnemiesCondition(i);
                            break;
                        }
                        if (scenarios.Count != 0 && scenarios[i].Condition == MyEnums.CombatConditionType.BossAtHalfHealth && !scenarios[i].ScenarioTriggered)
                        {
                            EvaluateBossAtHalfHealth(i);
                            break;
                        }
                    }
                }

                if (numberOfLivingEnemies <= 0 && battleStarted && scenarios.Count <= 0) { EndBattle(); }
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            //Probably calling it all the time when player is inside is not a good way to go
            //TODO consider checking player and enemy in arena by using their layers
            if (collision.GetComponent<Player>() != null && OnPlayerEntered != null) { OnPlayerEntered(true); }
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<Enemy>() != null) { collision.GetComponent<Enemy>().SetEnemyInCombatArea(true); }
        }
        void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.GetComponent<Player>() != null && OnPlayerEntered != null) { OnPlayerEntered(false); }
            if (collision.GetComponent<Enemy>() != null) { collision.GetComponent<Enemy>().SetEnemyInCombatArea(false); }
        }


        #region Private Methods
        private void EvaluateEnteredAreaCondtion(int i)
        {
            SpawnEnemies(i);
            scenarios[i].SetScenarioTriggered(true);
            scenarios.Remove(scenarios[i]);
        }
        private void EvaluateTimeCondtion(int i)
        {
            if (battleStartedTime + scenarios[i].SpawnTime <= Time.time)
            {
                SpawnEnemies(i);
                scenarios[i].SetScenarioTriggered(true);
                scenarios.Remove(scenarios[i]);
            }
        }
        private void EvaluateKillAllActiveEnemiesCondition(int i)
        {
            if (numberOfLivingEnemies <= 0)
            {
                SpawnEnemies(i);
                scenarios[i].SetScenarioTriggered(true);
                scenarios.Remove(scenarios[i]);
            }
        }
        private void EvaluateBossAtHalfHealth(int i)
        {
            if (bossReference.CurentHealth <= bossReference.MaxHealth * 0.5)
            {
                SpawnEnemies(i);
                scenarios[i].SetScenarioTriggered(true);
                scenarios.Remove(scenarios[i]);
            }
        }

        private void SpawnEnemies(int i)
        {
            foreach (var spawner in scenarios[i].SpawnersToTrigger)
            {
                GameObject spawnedCharacter = spawner.SpawnCharacter();
                if (spawnedCharacter.GetComponent<Boss>() != null)
                {
                    bossReference = spawnedCharacter.GetComponent<Boss>();
                }
                if (spawnedCharacter.GetComponent<Enemy>() != null)
                {
                    spawnedCharacter.GetComponent<Enemy>().OnEnemyDies += ctx => numberOfLivingEnemies--;
                    numberOfLivingEnemies++;
                }
            }
        }

        private void StartBattle()
        {
            if (!gameManager.CombatAreas[AreaID])
            {
                battleStarted = true;
                battleStartedTime = Time.time;
                foreach (var blocker in blockers) { blocker.SetActive(true); }
            }
        }
        private void EndBattle()
        {
            foreach (var blocker in blockers)
            {
                blocker.GetComponent<ParticleSystem>().Stop();
                blocker.GetComponent<Collider2D>().enabled = false;
                blocker.GetComponent<Light2D>().enabled = false;
            }

            gameManager.CombatAreas[AreaID] = true;
        }

        #endregion


        #region Public Methods
        public void SetAreaID(string value)
        {
            AreaID = value;
        }

        #endregion

    }

}
﻿using UnityEngine;
using MetalProject.Characters;

namespace MetalProject.Combat
{
    public class BattleStartTrigger : MonoBehaviour
    {
        public delegate void OnPlayerTriggersBattleDelegate();
        public event OnPlayerTriggersBattleDelegate OnPlayerTriggersBattle;

        public bool PlayerEntered { get; private set; }

        private void Awake()
        {
            PlayerEntered = false;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!PlayerEntered)
            {
                if (collision.GetComponent<Player>() != null && OnPlayerTriggersBattle != null)
                {
                    PlayerEntered = true;
                    OnPlayerTriggersBattle();
                }

            }
        }
    }
}
﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.CommonUtilty;

namespace MetalProject.Combat
{
    [System.Serializable]
    public class CombatScenario
    {
        [SerializeField] private CharacterSpawner[] spawnersToTrigger;
        [SerializeField] private BattleStartTrigger initialAreaTrigger;
        [SerializeField] private float spawnTime;
        [SerializeField] private MyEnums.CombatConditionType condition;

        public CharacterSpawner[] SpawnersToTrigger
        {
            get { return spawnersToTrigger; }
            private set { spawnersToTrigger = value; }
        }
        public BattleStartTrigger InitialAreaTrigger
        {
            get { return initialAreaTrigger; }
            private set { initialAreaTrigger = value; }
        }
        public float SpawnTime
        {
            get { return spawnTime; }
            private set { spawnTime = value; }
        }
        public MyEnums.CombatConditionType Condition
        {
            get { return condition; }
            private set { condition = value; }
        }
        public bool ScenarioTriggered { get; private set; }


        public void SetScenarioTriggered(bool value)
        {
            ScenarioTriggered = value;
        }
    }
}

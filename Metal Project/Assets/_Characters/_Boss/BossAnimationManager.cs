﻿using UnityEngine;
using MetalProject.CommonUtilty;
using MetalProject.Core;
using System;

namespace MetalProject.Characters
{
    public class BossAnimationManager : MonoBehaviour
    {
        Animator animator;
        AnimationParameters animParameters;
        SpriteRenderer spriteRenderer;
        CameraController cameraController;

        public delegate void OnSpawnFallingRocksDelagate();
        public event OnSpawnFallingRocksDelagate OnSpawnFallingRocks;

        public delegate void OnSpawnShockWaveDelagate();
        public event OnSpawnShockWaveDelagate OnSpawnShockWave;

        private struct AnimationParameters
        {
            public int Walk;
            public int AttackType;
            public int Charge;
            public int Attack;
            public int Death;
        }

        void Start()
        {
            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();

            cameraController = FindObjectOfType<CameraController>();
            animParameters = new AnimationParameters
            {
                Walk = Animator.StringToHash("Walk"),
                AttackType = Animator.StringToHash("AttackType"),
                Charge = Animator.StringToHash("Charge"),
                Attack = Animator.StringToHash("Attack"),
                Death = Animator.StringToHash("Death"),
            };
        }


        public void SetWalk(bool value)
        {
            animator.SetBool(animParameters.Walk, value);
        }
        public bool GetWalk()
        {
            return animator.GetBool(animParameters.Walk);
        }
        public void SetAttackType(int attackType)
        {
            animator.SetInteger(animParameters.AttackType, attackType);
        }
        public int GetAttackType()
        {
            return animator.GetInteger(animParameters.AttackType);
        }
        public void SetAttack()
        {
            animator.SetTrigger(animParameters.Attack);
        }
        public void SetDeath()
        {
            animator.SetTrigger(animParameters.Death);
        }

        #region Method raised by animation events

        public void PassSpawnFallingRocks()
        {
            OnSpawnFallingRocks();
        }
        public void PassSpawnShockWave()
        {
            OnSpawnShockWave();
        }
        public void ShakePlayerScreen(float magnitued)
        {
            cameraController.SetShakeParameters(0.3f, magnitued, 0.5f);
        }

        #endregion
    }
}
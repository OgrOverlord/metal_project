﻿using System.Collections;
using System.Collections.Generic;
using MetalProject.Characters;
using UnityEngine;

namespace MetalProject.Combat
{
    public class FirePortal : MonoBehaviour
    {
        public Vector2 DirectionToPlayer { get; private set; }

        [SerializeField] private Fireball fireball;

        private Player player;

        void Start()
        {
            player = FindObjectOfType<Player>();
        }
        void Update()
        {
            CalculateDirectionToPlayer();
        }

        public void SpawnFireball()
        {
            Fireball fireballInstance = Instantiate(fireball, transform.position, transform.rotation);
            fireballInstance.RotateFireball(DirectionToPlayer);
        }
        private void CalculateDirectionToPlayer()
        {
            DirectionToPlayer = (player.transform.position - this.transform.position).normalized;
        }
    }
}
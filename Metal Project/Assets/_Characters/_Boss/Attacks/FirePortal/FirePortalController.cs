﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetalProject.Combat
{
    public class FirePortalController : MonoBehaviour
    {
        [SerializeField] private float attackInterval;
        [SerializeField] private float spawnAttackDelay;
        [SerializeField] private string fireTriggerName;

        private Animator[] portals;
        private float spawnTime;
        private float lastAttackTime = 0f;

        void Start()
        {
            portals = GetComponentsInChildren<Animator>();
            spawnTime = Time.time;       
        }

        void Update()
        {
            if (spawnTime + spawnAttackDelay <= Time.time )
            {
                if (lastAttackTime + attackInterval <= Time.time)
                {
                    lastAttackTime = Time.time;
                    int random = Random.Range(0, portals.Length);
                    portals[random].SetTrigger(fireTriggerName);
                }

            }
        }

    }
}
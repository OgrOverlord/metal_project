﻿using UnityEngine;

namespace MetalProject.Combat
{
    [CreateAssetMenu(fileName = "New Boss Attack", menuName = "Attacks/Boss Attack")]
    public class BossAttack : ScriptableObject
    {
            [Header("Base Parameters")]
            [Range(-100, 0)] public int damage;
            public LayerMask layerMask;
            public bool unblockable;   
    }
}
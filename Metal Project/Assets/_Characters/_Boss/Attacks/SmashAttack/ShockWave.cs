﻿using UnityEngine;
using MetalProject.CommonUtilty;
using MetalProject.Characters;

namespace MetalProject.Combat
{
    public class ShockWave : Projectile
    {
        override protected void Start()
        {
            base.Start();
            TargetCanBeDamaged = true; 
        }    
        override protected void OnTriggerEnter2D(Collider2D collision)
        {
            if (HelperMethods.CheckIfLayerIsInLayerMask(collision.gameObject.layer, IncludeLayerMask))
            {
                if (collision.gameObject.GetComponent<PlayerAnimationManager>() != null && !collision.gameObject.GetComponent<PlayerAnimationManager>().GetDashing())
                {
                    DealDamage(collision);
                }
            }
        }
     
        public void SetDirectionAndScale(Vector2 direction)
        {
            Direction = direction;
            transform.localScale = new Vector3(direction.x, 1, 1f);
        }
    }

}
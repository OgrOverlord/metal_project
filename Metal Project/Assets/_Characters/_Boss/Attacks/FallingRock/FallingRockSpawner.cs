﻿using UnityEngine;

namespace MetalProject.Combat
{
    public class FallingRockSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject fallingRock;
        [SerializeField] private Transform[] spawnerReferencePoints;
        [SerializeField] private GameObject warrningMarker;
        [SerializeField] private LayerMask maskForWarrningMarker;


        public void SpawnRocks(int number)
        {
            for (int i = 0; i < number; i++)
            {
                Vector3 spawnPoint = new Vector3(Random.Range(spawnerReferencePoints[0].position.x, spawnerReferencePoints[1].position.x), spawnerReferencePoints[0].position.y);
                RaycastHit2D hit = Physics2D.Raycast(spawnPoint, Vector2.down, 50f, maskForWarrningMarker);
                if (hit) { Instantiate(warrningMarker, hit.point, Quaternion.identity); }
                Instantiate(fallingRock, spawnPoint, Quaternion.identity);
            }
        }
    }
}
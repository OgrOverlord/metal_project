﻿using UnityEngine;
using MetalProject.CommonUtilty;
using MetalProject.Characters;

namespace MetalProject.Combat
{
    public class FallingRock : Projectile
    {
        [Header("Sprites")]
        [SerializeField] Sprite[] sprites;
  
        private float rockRotation;
        private SpriteRenderer spriteRenderer;

        override protected void Start()
        {
            base.Start();
            spriteRenderer = GetComponent<SpriteRenderer>();

            spriteRenderer.sprite = sprites[Random.Range(0, 3)];
            TargetCanBeDamaged = true;

            Velocity += Random.Range(-2f, 3f);
            Direction = new Vector2(0, -1);
        }
        override protected void OnTriggerEnter2D(Collider2D collision)
        {
            if (HelperMethods.CheckIfLayerIsInLayerMask(collision.gameObject.layer, IncludeLayerMask))
            {
                if (collision.gameObject.GetComponent<PlayerAnimationManager>() != null && !collision.gameObject.GetComponent<PlayerAnimationManager>().GetDashing())
                {
                    Velocity = 0f;
                    animator.SetTrigger(collisionTrigger);
                    DealDamage(collision);
                }
            }
            if (HelperMethods.CheckIfLayerIsNOTInLayerMask(collision.gameObject.layer, IgnoreLayerMask))
            {
                Velocity = 0f;
                animator.SetTrigger(collisionTrigger);
                DestroyProjectile();
            }
        }

    }
}
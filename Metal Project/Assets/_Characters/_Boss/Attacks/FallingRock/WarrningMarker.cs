﻿using UnityEngine;

namespace MetalProject.Combat
{
    public class WarrningMarker : MonoBehaviour
    {
        public void DetroyMarker()
        {
            Destroy(this.gameObject);
        } 

    }
}
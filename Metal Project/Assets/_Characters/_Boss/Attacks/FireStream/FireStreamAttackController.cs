﻿using UnityEngine;
using MetalProject.CommonUtilty;
using MetalProject.Characters;

namespace MetalProject.Combat
{
    public class FireStreamAttackController : MonoBehaviour
    {
        [SerializeField] private BossAttack attackData;

        private void OnParticleCollision(GameObject other)
        {
            if (other.layer == HelperMethods.LayerMaskToLayer(attackData.layerMask))
            {
                if (other.GetComponent<ICharacter>() != null) { DealDamage(attackData.damage, other); }
            }
        }

        private void DealDamage(int damage, GameObject target)
        {
            ICharacter character = target.GetComponent<ICharacter>();

            if (character != null && !character.IsInvincable)
            {
                if (!character.IsDead) { character.ModifyHealthBy(damage); }
            }
        }

    }
}
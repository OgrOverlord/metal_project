﻿using UnityEngine;
using MetalProject.CommonUtilty;

namespace MetalProject.Characters
{
    public class BossSensColliders : MonoBehaviour
    {
        [SerializeField] private bool front;
        [SerializeField] private bool mid;
        [SerializeField] private bool back;

        [SerializeField] private LayerMask searchMask;

        public bool Front
        {
            get { return front; }
            set { front = value; }
        }
        public bool Mid
        {
            get { return mid; }
            set { mid = value; }
        }
        public bool Back
        {
            get { return back; }
            set { back = value; }
        }
        public bool PlayerPresent { get; private set; }

        void Start()
        {
            PlayerPresent = false;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (HelperMethods.CheckIfLayerIsInLayerMask(collision.gameObject.layer, searchMask)) { PlayerPresent = true; }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (HelperMethods.CheckIfLayerIsInLayerMask(collision.gameObject.layer, searchMask)) { PlayerPresent = false; }
        }
    }
}
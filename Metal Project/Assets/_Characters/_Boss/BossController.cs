﻿using UnityEngine;
using MetalProject.Core;
using MetalProject.CommonUtilty;

namespace MetalProject.Characters
{
    [SelectionBase]
    public class BossController : Controler2D
    {
        #region Serialzied field
        [SerializeField] private float chaseOffset;
        [SerializeField] private float turnOffset;
        [SerializeField] private float walkSpeed;

        #endregion

        #region fields
        RaycastControler raycastControler;
        BossAnimationManager bossAnimationManager;
        Boss boss;
        Player player;

        Vector3 velocity;
        float gravity;

        #endregion

        #region properties
        public float DistanceToPlayerX { get; private set; }
        public float DistanceToPlayerY { get; private set; }
        public Vector3 DirectionToPlayer { get; private set; }

        #endregion


        new void Start()
        {
            base.Start();
            raycastControler = GetComponent<RaycastControler>();
            bossAnimationManager = GetComponent<BossAnimationManager>();
            boss = GetComponent<Boss>();

            gravity = -56f;

            player = FindObjectOfType<Player>();
        }
        private void Update()
        {
            CalculateDistanceToPlayerX();
            CalculateDistanceToPlayerY();
            CalculateDirectionToPlayer();

            if (!boss.IsDead)
            {
                if (DistanceToPlayerX > chaseOffset && bossAnimationManager.GetAttackType() != (int)MyEnums.BossAttackType.FireStreamAttack) { bossAnimationManager.SetWalk(true); }
                else { bossAnimationManager.SetWalk(false); }

                TurnToFacePlayer();

                SetVelocityX();
                SetVelocityY();

                Move(velocity * Time.deltaTime);
            }
        }

        protected override void SetVelocityX()
        {
            if (bossAnimationManager.GetWalk()) { velocity.x = MovmentDirection * walkSpeed; }
            else { velocity.x = 0f; }
        }
        protected override void SetVelocityY()
        {
            if ((collisionControler.Above || collisionControler.Below))
            {
                velocity.y = 0;
            }
            else
            {
                velocity.y += gravity * Time.deltaTime;
            }
        }

        private void CalculateDistanceToPlayerX()
        {
            Vector2 temp = player.transform.position;
            Vector2 temp2 = this.transform.position;

            temp.y = 0;
            temp2.y = 0;

            DistanceToPlayerX = Mathf.Abs(Vector2.Distance(temp, temp2));
        }
        private void CalculateDistanceToPlayerY()
        {
            Vector2 temp = player.transform.position;
            Vector2 temp2 = this.transform.position;

            temp.x = 0;
            temp2.x = 0;

            DistanceToPlayerY = Mathf.Abs(Vector2.Distance(temp, temp2));
        }
        private void CalculateDirectionToPlayer()
        {
            DirectionToPlayer = (player.transform.position - this.transform.position).normalized;
        }
        private void TurnToFacePlayer()
        {
            if (DistanceToPlayerX >= turnOffset)
            {
                if (DirectionToPlayer.x < 0)
                {
                    transform.localScale = new Vector3(1f, 1f, 1f);
                    SetMovmentDirection(Vector2.left.x);
                }
                if (DirectionToPlayer.x > 0)
                {
                    transform.localScale = new Vector3(-1f, 1f, 1f);
                    SetMovmentDirection(Vector2.right.x);
                }
            }
        }

        public void SetMovmentDirection(float value)
        {
            MovmentDirection = value;
        }
    }
}
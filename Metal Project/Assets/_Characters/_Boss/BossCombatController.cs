﻿using UnityEngine;
using MetalProject.CommonUtilty;
using MetalProject.Characters;
using System;

namespace MetalProject.Combat
{
    public class BossCombatController : MonoBehaviour
    {
        [SerializeField] private BossAttack[] attackArray;
        [SerializeField] private ShockWave shockWave;
        [SerializeField] private Transform shockWaveSpawnPoint;
        [SerializeField] private float firestreamTriggerRange;

        public bool TargetCanBeDamaged { get; private set; }
        public BossAttack CurrentAttack { get; private set; }

        private BossSensColliders[] bossSensColliders;
        private BossSensColliders playerOccupiedArea;
        private FallingRockSpawner fallingRockSpawner;
        private BossController bossController;
        private Boss boss;

        private BossAnimationManager animationManager;

        void Start()
        {
            SetTargetCanBeDamaged(true);
            bossSensColliders = GetComponentsInChildren<BossSensColliders>();
            animationManager = GetComponentInParent<BossAnimationManager>();
            fallingRockSpawner = GetComponentInChildren<FallingRockSpawner>();
            bossController = GetComponentInParent<BossController>();
            boss = GetComponentInParent<Boss>();

            animationManager.OnSpawnFallingRocks += SpawnFallingRocks;
            animationManager.OnSpawnShockWave += SpawnShockWave;
        }

        void Update()
        {
            foreach (var sensCollider in bossSensColliders)
            {
                if (sensCollider.PlayerPresent) { playerOccupiedArea = sensCollider; }
            }

            if (!boss.IsDead)
            {
                if (playerOccupiedArea)
                {
                    if (playerOccupiedArea.Front && playerOccupiedArea.PlayerPresent) { PerformAttack(MyEnums.BossAttackType.SmashAttack); }
                    if (playerOccupiedArea.Mid && playerOccupiedArea.PlayerPresent) { PerformAttack(MyEnums.BossAttackType.StompAttack); }
                    if ((!playerOccupiedArea.PlayerPresent && bossController.DistanceToPlayerX >= firestreamTriggerRange) ||
                         (bossController.DistanceToPlayerY >= firestreamTriggerRange))
                    {
                        PerformAttack(MyEnums.BossAttackType.FireStreamAttack);
                    }
                }
            }
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == HelperMethods.LayerMaskToLayer(CurrentAttack.layerMask))
            {
                if (collision.GetComponent<ICharacter>() != null)
                {
                    DealDamage(CurrentAttack.damage, collision);
                }
            }
        }

        private void DealDamage(int damage, Collider2D target)
        {
            ICharacter character = target.GetComponent<ICharacter>();

            if (character != null && !character.IsInvincable)
            {
                if (TargetCanBeDamaged && !character.IsDead)
                {
                    character.ModifyHealthBy(damage);
                }
            }
        }
        private void PerformAttack(MyEnums.BossAttackType attackType)
        {
            animationManager.SetAttackType((int)attackType);
            CurrentAttack = attackArray[(int)attackType - 1];
            animationManager.SetAttack();
        }

        private void SpawnFallingRocks()
        {
            fallingRockSpawner.SpawnRocks(UnityEngine.Random.Range(4, 8));
        }
        private void SpawnShockWave()
        {
            ShockWave shockwave = Instantiate(shockWave, shockWaveSpawnPoint.position, transform.rotation);
            shockwave.SetDirectionAndScale(new Vector2(bossController.MovmentDirection, 0f));
        }

        public void SetTargetCanBeDamaged(bool value)
        {
            TargetCanBeDamaged = value;
        }
    }
}
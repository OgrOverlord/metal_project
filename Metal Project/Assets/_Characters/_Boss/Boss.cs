﻿using System.Collections;
using UnityEngine;
using MetalProject.UI;
using UnityEngine.UI;

namespace MetalProject.Characters
{
    public class Boss : MonoBehaviour, ICharacter
    {
        #region Serialzied fields
        [SerializeField] [Range(100, 1500)] int maxHealth;
        [SerializeField] [Range(0, 500)] int maxStamina;

        [SerializeField] GameObject firePortals; //TODO in fure probabbyl should be place in other script 
        [SerializeField] [Range(0f, 1f)] float firePortalsActivationThreshold; 

        #endregion

        GameObject bossUI;
        Slider bossHealthBarSlider;
        BossAnimationManager bossAnimationManager;

        #region Properties
        public int MaxHealth
        {
            get { return maxHealth; }
            private set { maxHealth = value; }
        }
        public int CurentHealth { get; private set; }
        public int MaxStamina
        {
            get { return maxStamina; }
            private set { maxStamina = value; }
        }
        public int CurentStamina { get; private set; }

        public bool IsInvincable { get; private set; }
        public bool IsDead { get; private set; }

        #endregion
        void Start()
        {
            CurentHealth = maxHealth;
            CurentStamina = maxStamina;
            IsInvincable = false;
            bossAnimationManager = GetComponent<BossAnimationManager>();

            SetupBossHealthBar(); //TODO since inactive objects cannot be referenced there must be some way for this to invisible before entering boss arena
        }


        public void Die()
        {
            IsDead = true;
            bossHealthBarSlider.gameObject.SetActive(false);
            SetIsInvincable(true);
            bossAnimationManager.SetDeath();
            StartCoroutine(DestroyEnemyAfterDelay());
        }
        public void ModifyHealthBy(int value)
        {
            CurentHealth += value;
            bossHealthBarSlider.value = CurentHealth;

            if (CurentHealth <= 0) { Die(); }
            if ((((float)CurentHealth / (float)maxHealth) <= firePortalsActivationThreshold) && !firePortals.activeSelf)
            {
                firePortals.SetActive(true);
            }
        }
        public void ModifyStaminaBy(int value)
        {
            CurentStamina += value;
            if (CurentStamina <= 0) { CurentStamina = maxStamina; }
        }
        public void SetHealthTo(int value)
        {
            CurentHealth = value;
        }
        public void SetIsInvincable(bool value)
        {
            IsInvincable = value;
        }

        IEnumerator DestroyEnemyAfterDelay()
        {
            yield return new WaitForSeconds(10f);
            Destroy(this.gameObject);
        }

        private void SetupBossHealthBar()
        {
            bossUI = FindObjectOfType<BossUI>().gameObject;
            bossHealthBarSlider = bossUI.GetComponentInChildren<Slider>();
            bossHealthBarSlider.gameObject.SetActive(true);

            bossHealthBarSlider.maxValue = MaxHealth;
            bossHealthBarSlider.value = CurentHealth;
        }
    }
}
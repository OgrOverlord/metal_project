﻿using UnityEngine;

namespace MetalProject.Characters
{
    public class Slowdown : StatusEffect
    {
        PlayerControler playerControler;

        public Slowdown(StatusEffectData statusEffctData, GameObject gameObject) : base(statusEffctData, gameObject)
        {
            playerControler = gameObject.GetComponent<PlayerControler>();
        }

        public override void ApplyEffect()
        {
            SlowdownData slowdownData = StatusEffectData as SlowdownData;
            playerControler.SetMovmentSpeedTo(slowdownData.speedDecresesTo);
        }

        public override void End()
        {
            SlowdownData slowdownData = StatusEffectData as SlowdownData;
            playerControler.ResetMovmentSpeedToDefault();
        }
    }
}
﻿using UnityEngine;

namespace MetalProject.Characters
{
    [CreateAssetMenu(menuName = "Status Effects/Slowdown")]
    public class SlowdownData : StatusEffectData
    {
        public float speedDecresesTo;
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MetalProject.Characters
{
    public class StatusEffectController : MonoBehaviour
    {
        private Dictionary<StatusEffectData ,StatusEffect> ActiveStatusEffects = new Dictionary<StatusEffectData, StatusEffect>();

        void Update()
        {
            if (ActiveStatusEffects.Count != 0)
            {
                foreach (var status in ActiveStatusEffects.Values.ToList()) // to iterate over dictonary I have to change it to list 
                {
                    status.Tick(Time.deltaTime);

                    if (status.isFinished)
                    {
                        ActiveStatusEffects.Remove(status.StatusEffectData); //Removing is based on keys, keys are serialzied data files
                    }   
                }
            }
        }

        public void AddStatus(StatusEffect statusEffect)
        {
            if (!ActiveStatusEffects.ContainsKey(statusEffect.StatusEffectData))
            {
                ActiveStatusEffects.Add(statusEffect.StatusEffectData, statusEffect);
                statusEffect.ApplyEffect();
            }
        }
    }
}
﻿using UnityEngine;

namespace MetalProject.Characters
{
    [System.Serializable]
    public abstract class StatusEffect
    {
        public StatusEffectData StatusEffectData { get; }

        public float remaningDuration;
        public bool isFinished;

        protected readonly GameObject target;

        public StatusEffect(StatusEffectData statusEffctData, GameObject gameObject)
        {
            StatusEffectData = statusEffctData;
            target = gameObject;
            remaningDuration = statusEffctData.duration;
            isFinished = false; 
        }

        public virtual void Tick(float delta)
        {
            remaningDuration -= delta;

            if (remaningDuration <= 0)
            {
                End();
                isFinished = true;
            }
        }
        public abstract void ApplyEffect();
        public abstract void End();

    }
}
﻿using UnityEngine;

namespace MetalProject.Characters 
{
    public class Exhaustion : StatusEffect
    {
        Player player;
        ExhaustionData exhaustionData;
        GameObject screenObsureEffectInstance;

        public Exhaustion(StatusEffectData statusEffctData, GameObject gameObject) : base(statusEffctData, gameObject)
        {
            player = gameObject.GetComponent<Player>();
            exhaustionData = StatusEffectData as ExhaustionData;
        }

        public override void ApplyEffect()
        {
            player.SetStaminaRegenBlocked(true);
            screenObsureEffectInstance = Object.Instantiate(exhaustionData.screenObsureEffect);
        }

        public override void End()
        {
            player.SetStaminaRegenBlocked(false);
            Object.Destroy(screenObsureEffectInstance);
        }
    }
}

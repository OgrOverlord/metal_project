﻿using UnityEngine;

namespace MetalProject.Characters
{
    [CreateAssetMenu(menuName = "Status Effects/Exhaustio")]
    public class ExhaustionData : StatusEffectData
    {
        public GameObject screenObsureEffect; 
    }
}

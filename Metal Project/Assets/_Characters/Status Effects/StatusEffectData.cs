﻿using UnityEngine;

namespace MetalProject.Characters
{
    public abstract class StatusEffectData : ScriptableObject
    {
        public float duration;
    }
}
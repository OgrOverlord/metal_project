﻿using UnityEngine;

namespace MetalProject.Characters
{
    [CreateAssetMenu(menuName = "Status Effects/Burning")]
    public class BurningData : StatusEffectData
    {
        public GameObject particleEffects;
        public int damagePerTick;
        public float damageFrequency;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetalProject.Characters
{
    public class Burning : StatusEffect
    {
        private Player player;
        private GameObject spawnedParticles;
        private float tillDamageTick;
        private BurningData burningData;

        public Burning(StatusEffectData statusEffctData, GameObject gameObject) : base(statusEffctData, gameObject)
        {
            player = gameObject.GetComponent<Player>();
            burningData = StatusEffectData as BurningData;
            tillDamageTick = burningData.damageFrequency;
        }


        public override void Tick(float delta)
        {
            tillDamageTick -= delta;
            if (tillDamageTick <= 0)
            {
                if (!player.IsInvincable && !player.IsDead) { player.ModifyHealthBy(burningData.damagePerTick); }
                tillDamageTick = burningData.damageFrequency;
            }

            base.Tick(delta);
        }

        public override void ApplyEffect()
        {
            spawnedParticles = Object.Instantiate(burningData.particleEffects, target.transform.position, Quaternion.identity, target.transform);
        }

        public override void End()
        {
            Object.Destroy(spawnedParticles, 0.5f);
        }
    }
}
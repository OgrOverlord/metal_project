﻿using UnityEngine;
using UnityEngine.UI;

namespace MetalProject.Characters
{
    public class CharacterSpawner : MonoBehaviour
    {
        public bool PlayerSpawner
        {
            get { return isPlayerSpawner; }
            private set { isPlayerSpawner = value; }
        }

        [SerializeField] private GameObject characterToSpawn;
        [SerializeField] private bool isPlayerSpawner;

        private float debugSphereRadius;

        public GameObject SpawnCharacter()
        {
            if (characterToSpawn.GetComponent<ICharacter>() != null)
            {
                //Since player spawn point can be moved using the checkpoint shrines player shold not be parrented to its spawner
                if (isPlayerSpawner) { return Instantiate(characterToSpawn, transform.position, transform.rotation); }
                else { return Instantiate(characterToSpawn, transform); }
            }
            return null;
        }

        private void OnDrawGizmos()
        {
            if (characterToSpawn.GetComponent<Player>())
            {
                Gizmos.color = Color.green;
                debugSphereRadius = 1f;
            }
            else if (characterToSpawn.GetComponent<Enemy>())
            {
                Gizmos.color = Color.red;
                debugSphereRadius = 1f;
            }
            else if (characterToSpawn.GetComponent<Boss>())
            {
                Gizmos.color = Color.yellow;
                debugSphereRadius = 2.5f;
            }
            Gizmos.DrawWireSphere(this.gameObject.transform.position, debugSphereRadius);
        }
    }
}
﻿using UnityEngine;
using MetalProject.Characters;

namespace MetalProject.Combat
{
    public abstract class Projectile : MonoBehaviour
    {
        #region SerializedFields
        [Header("General parameters")]
        [SerializeField] [Range(-200, 0)] protected int damage;
        [SerializeField] protected float velocity;

        [Header("Special parameters")]
        [SerializeField] protected LayerMask includeLayerMask;
        [SerializeField] protected LayerMask ignoreLayerMask;
        [SerializeField] protected float specialRaysLenght;
        [SerializeField] protected float destroyDelay;

        [Header("Animation paramters")]
        [SerializeField] protected string collisionTrigger;

        #endregion

        #region Properties

        public int Damage
        {
            get { return damage; }
            private set { damage = value; }
        }
        public float Velocity
        {
            get { return velocity; }
            protected set { velocity = value; }
        }
        public Vector2 Direction { get; protected set; }

        public Vector2 DamageRayDirection { get; protected set; }

        public LayerMask IncludeLayerMask
        {
            get { return includeLayerMask; }
            private set { includeLayerMask = value; }
        }
        public LayerMask IgnoreLayerMask
        {
            get { return ignoreLayerMask; }
            private set { ignoreLayerMask = value; }
        }
        public RaycastHit2D FrontHitCheck { get; protected set; }
        public float SpecialRaysLenght
        {
            get { return specialRaysLenght; }
            private set { specialRaysLenght = value; }
        }
        public bool TargetCanBeDamaged { get; protected set; }

        public bool Destroyed { get; protected set; }
        #endregion

        #region fields
        protected float timeOfSpawning;
        protected Animator animator;

        #endregion

        virtual protected void Start()
        {
            animator = GetComponent<Animator>();
            timeOfSpawning = Time.time;
            Destroyed = false;
        }
        virtual protected void Update()
        {
            if (timeOfSpawning + destroyDelay <= Time.time) { DestroyProjectile(); }
            this.gameObject.transform.Translate(Velocity * Direction * Time.deltaTime);
        }

        abstract protected void OnTriggerEnter2D(Collider2D collision);

        protected void DealDamage(Collider2D target)
        {
            ICharacter character = target.GetComponent<ICharacter>();
            if (character != null && !character.IsInvincable && TargetCanBeDamaged) { character.ModifyHealthBy(Damage); }
            DestroyProjectile();
        }
        protected void SendDamageCheckRay()
        {
            FrontHitCheck = Physics2D.Raycast(gameObject.transform.position, DamageRayDirection * specialRaysLenght, specialRaysLenght, includeLayerMask);
            Debug.DrawRay(gameObject.transform.position, DamageRayDirection * specialRaysLenght, Color.yellow);
        }
        protected void CheckIfTargetCanBeDamaged()
        {
            if (FrontHitCheck.collider != null)
            {
                if (FrontHitCheck.collider.gameObject.GetComponent<PlayerShield>()) { TargetCanBeDamaged = false; }
                else { TargetCanBeDamaged = true; }
            }
        }
        protected void DestroyProjectile()
        {
            Destroyed = true;
            Destroy(this.gameObject, destroyDelay);
        }

    }
}

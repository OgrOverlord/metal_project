﻿namespace MetalProject.Characters
{
    public interface ICharacter 
    {
        int MaxHealth { get;}
        int CurentHealth { get;}
        int MaxStamina { get; }
        int CurentStamina { get; }

        bool IsInvincable { get;}
        bool IsDead { get;}

        void ModifyHealthBy(int value);
        void SetHealthTo(int value);
        void ModifyStaminaBy(int value);
        void SetIsInvincable(bool value);
        void Die();
    }
}
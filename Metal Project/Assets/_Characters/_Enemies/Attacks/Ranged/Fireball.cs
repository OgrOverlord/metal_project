﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.CommonUtilty;

namespace MetalProject.Combat
{
    public class Fireball : Projectile
    {
        public float FireballRotation { get; private set; }

        private AudioSource localAudioSource;

        override protected void Start()
        {
            base.Start();
            localAudioSource = GetComponent<AudioSource>();
            Direction = Vector2.right;    
        }

        override protected void Update()
        {
            base.Update();
            if (Destroyed) { localAudioSource.Stop(); } //NOTE SFXController changes volume and check if player is within hearing range, this stop sfx from playing when fireball is destroyed
            SendDamageCheckRay();
            CheckIfTargetCanBeDamaged();
        }
        override protected void OnTriggerEnter2D(Collider2D collision)
        {
            if (HelperMethods.CheckIfLayerIsInLayerMask(collision.gameObject.layer, IncludeLayerMask))
            {
                if (collision.gameObject.GetComponent<PlayerAnimationManager>() != null && !collision.gameObject.GetComponent<PlayerAnimationManager>().GetDashing())
                {
                    Velocity = 0f;
                    animator.SetTrigger(collisionTrigger);
                    DealDamage(collision);
                }
            }
            if (HelperMethods.CheckIfLayerIsNOTInLayerMask(collision.gameObject.layer, IgnoreLayerMask))
            {
                Velocity = 0f;
                animator.SetTrigger(collisionTrigger);
                DestroyProjectile();
            }
        }
  
        public void RotateFireball(Vector2 directionToTarget)
        {
            float angle = Vector2.Angle(directionToTarget, Vector2.up);
            if (directionToTarget.x < 0) { transform.Rotate(0f, 0f, 90 + angle); }
            else { transform.Rotate(0f, 0f, 90 - angle); }
            DamageRayDirection = directionToTarget;
        }
    }
}
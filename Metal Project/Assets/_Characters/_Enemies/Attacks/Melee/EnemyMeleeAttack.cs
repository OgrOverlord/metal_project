﻿using UnityEngine;

namespace MetalProject.Combat
{
    [CreateAssetMenu(fileName = "New Enemy Attack", menuName = "Attacks/Enemy Attack")]
    public class EnemyMeleeAttack : ScriptableObject
    {
        [Header("Base Parameters")]
        [Range(-100, 0)] public int damage;
        public LayerMask layerMask;
        public bool unblockable;

        [Header("Movment Parameters")]
        public bool attackHasMovment;
        public float movmentDuration;
        public float movmentDelay;
        public Vector2 movmentDistance;
    }
}
﻿using UnityEngine;
using MetalProject.CommonUtilty;


namespace MetalProject.Characters
{
    public class EnemyAnimationManager : MonoBehaviour
    {

        public struct ShaderProperties
        {
            public string Visibilty;
            public string FadeColor;
            public string OutlineThickness;
            public string OutlineColor;
        }
        private struct AnimationParameters
        {
            public int Patroling;
            public int Chasing;
            public int TakingDamage;
            public int Fighting;
            public int Attack;
            public int Dying;
            public int AttackNumber;
            public int Blocking;
            public int FireBlastCharging;
            public int AttackBlocked;
            public int Falling;
            public int Teleport;
        }

        Animator animator;
        AnimationParameters animParameters;
        ShaderProperties shaderProperties;
        SpriteRenderer spriteRenderer;
        Material material;

        public delegate void OnSpawnFireballDelegate();
        public event OnSpawnFireballDelegate OnSpawnFireball;

        public delegate void OnEnemyTeleportsDelegate();
        public event OnEnemyTeleportsDelegate OnEnemyTeleports;

        void Start()
        {
            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            material = spriteRenderer.material;

            animParameters = new AnimationParameters
            {
                Patroling = Animator.StringToHash("Patroling"),
                TakingDamage = Animator.StringToHash("TakingDamage"),
                Chasing = Animator.StringToHash("Chasing"),
                Fighting = Animator.StringToHash("Figthing"),
                Attack = Animator.StringToHash("Attack"),
                Dying = Animator.StringToHash("Dying"),
                AttackNumber = Animator.StringToHash("AttackNumber"),
                Blocking = Animator.StringToHash("Blocking"),
                FireBlastCharging = Animator.StringToHash("FireBlastCharging"),
                AttackBlocked = Animator.StringToHash("AttackBlocked"),
                Falling = Animator.StringToHash("Falling"),
                Teleport = Animator.StringToHash("Teleport"),
            };

            shaderProperties = new ShaderProperties
            {
                Visibilty = "_Visibilty",
                FadeColor = "_FadeColor",
                OutlineThickness = "_OutlineThickness",
                OutlineColor = "_OutlineColor"
            };
        }

        public void ChangeSpriteSortingLayerOrder(int layer)
        {
            spriteRenderer.sortingOrder = layer;
        }


        public void SetChasing(bool value)
        {
            animator.SetBool(animParameters.Chasing, value);
        }
        public bool GetChasing()
        {
            return animator.GetBool(animParameters.Chasing);
        }

        public void SetFigthing(bool value)
        {
            animator.SetBool(animParameters.Fighting, value);
        }
        public bool GetFigthing()
        {
            return animator.GetBool(animParameters.Fighting);
        }

        public void SetFalling(bool value)
        {
            animator.SetBool(animParameters.Falling, value);
        }
        public bool GetFalling()
        {
            return animator.GetBool(animParameters.Falling);
        }

        public void SetTakingDamage()
        {
            animator.SetTrigger(animParameters.TakingDamage);
        }
        public void SetDying()
        {
            animator.SetTrigger(animParameters.Dying);
        }

        public void SetBlocking()
        {
            animator.SetTrigger(animParameters.Blocking);
        }

        public void SetFireBlastCharging(bool value)
        {
            animator.SetBool(animParameters.FireBlastCharging, value);
        }
        public bool GetFireBlastCharging()
        {
            return animator.GetBool(animParameters.FireBlastCharging);
        }

        public void SetAttackBlocked()
        {
            animator.SetTrigger(animParameters.AttackBlocked);
        }
        public void SetTeleport(bool value)
        {
            animator.SetBool(animParameters.Teleport, value);
        }
        public bool GetTeleport()
        {
            return animator.GetBool(animParameters.Teleport);
        }

        public void SetAttackNumber(MyEnums.EnemyAttack value)
        {
            animator.SetInteger(animParameters.AttackNumber, (int)value);
        }

        public void SetOutlineThicknessShaderProperty(float value)
        {
            spriteRenderer.sharedMaterial.SetFloat(shaderProperties.OutlineThickness, value);
        }

        #region Method Raised by animation events

        public void PassPerformTeleportEvent()
        {
            OnEnemyTeleports();
        }
        public void PassSpawnFireballEvent()
        {
            OnSpawnFireball();
        }
        #endregion
    }


}
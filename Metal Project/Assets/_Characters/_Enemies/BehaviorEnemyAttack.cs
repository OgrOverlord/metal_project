﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.CommonUtilty;

namespace MetalProject.Combat
{
    public class BehaviorEnemyAttack : StateMachineBehaviour
    {
        [SerializeField] private EnemyMeleeAttack attackData;

        private EnemyControler enemyControler;
        private Enemy enemy;
        private EnemyAnimationManager enemyAnimationManager;
        private EnemyCombatControler enemyCombatControler;
        private bool componentsAreSet = false;

        private bool attackVelocitySet;
        private float movementStartTime;
        private float attackAnimationStartTime;

        private void InitialComponentSetup(Animator animator)
        {
            enemyControler = animator.gameObject.GetComponent<EnemyControler>();
            enemyAnimationManager = animator.gameObject.GetComponent<EnemyAnimationManager>();
            enemyCombatControler = animator.gameObject.GetComponentInChildren<EnemyCombatControler>();
            enemy = animator.gameObject.GetComponent<Enemy>();
            componentsAreSet = true;
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!componentsAreSet) { InitialComponentSetup(animator); }
            enemyCombatControler.SetCurrentAttackData(attackData);
            enemyCombatControler.SetEnemyIsAttacking(true);
            attackAnimationStartTime = Time.time;
            attackVelocitySet = false;

            if (attackData.name == "Fire_Blast_Attack") { enemyAnimationManager.ChangeSpriteSortingLayerOrder(3); }
            if (attackData.name == "Spawn_Attack")
            {
                enemy.SetCanBeStuned(false);
                enemy.SetIsInvincable(true);
                enemyAnimationManager.ChangeSpriteSortingLayerOrder(3);
            }
        }
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            CheckIfPlayerHasShieldUp();

            if (!attackVelocitySet && attackData.attackHasMovment && attackAnimationStartTime + attackData.movmentDelay <= Time.time)
            {
                attackVelocitySet = true;
                movementStartTime = Time.time;
                enemyControler.SetAttackMovmentVelocity(attackData.movmentDuration, attackData.movmentDistance);
            }

            if (movementStartTime + attackData.movmentDuration <= Time.time) { enemyControler.ResetAttackMovmentVelocity(); }
        }
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            enemyCombatControler.SetTargetCanBeDamaged(true);
            enemy.SetCanBeStuned(true);

            if (enemy.AttacksTaken < enemy.ConcurentAttacksForBlock) { enemy.SetIsInvincable(false); } //NOTE condtion added so that enemy's block invicibilty is not  reset by interrupted attack

            enemyAnimationManager.ChangeSpriteSortingLayerOrder(1);
            enemyCombatControler.SetEnemyIsAttacking(false);

            if (attackData.name == "Fire_Blast_Attack") { enemyAnimationManager.SetFireBlastCharging(false); }
        }

        private void CheckIfPlayerHasShieldUp()
        {
            if (enemyControler.FrontHitCheck.collider != null)
            {
                if (enemyControler.FrontHitCheck.collider.gameObject.GetComponent<PlayerShield>() && !attackData.unblockable) { enemyCombatControler.SetTargetCanBeDamaged(false); }
                else { enemyCombatControler.SetTargetCanBeDamaged(true); }
            }
        }
    }
}

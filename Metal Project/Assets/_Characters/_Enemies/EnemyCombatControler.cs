﻿using UnityEngine;
using MetalProject.CommonUtilty;
using MetalProject.Characters;

namespace MetalProject.Combat
{
    public class EnemyCombatControler : MonoBehaviour
    {
        [Header("Projectiels")]
        [SerializeField] private GameObject fireball;

        [Header("Attack Distances")]
        [SerializeField] [Range(0,20)] private float fireballXDistance;
        [SerializeField] [Range(0, 20)] private float YDistanceMax;
        [SerializeField] [Range(0, 20)] private float YDistanceMin;
        [SerializeField] [Range(0, 20)] private float fireDashXDistanceMax;
        [SerializeField] [Range(0, 20)] private float fireDashXDistanceMin;


        EnemyAnimationManager enemyAnimationManager;
        EnemyControler enemyControler;
        Enemy enemy;

        public EnemyMeleeAttack CurrentAttack { get; private set; }
        public bool EnemyIsAttacking { get; private set; }
        public bool TargetCanBeDamaged { get; private set; }

        void Start()
        {
            TargetCanBeDamaged = true;
            enemyControler = GetComponentInParent<EnemyControler>();
            enemyAnimationManager = GetComponentInParent<EnemyAnimationManager>();
            enemy = GetComponentInParent<Enemy>();
            enemyAnimationManager.OnSpawnFireball += SpawnFireball;
        }
        void Update()
        {
            PickAttackType();
        }

        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == HelperMethods.LayerMaskToLayer(CurrentAttack.layerMask))
            {
                if (collision.GetComponent<ICharacter>() != null)
                {
                    DealDamage(CurrentAttack.damage, collision);
                }
            }
        }

        private void DealDamage(int damage, Collider2D target)
        {
            ICharacter character = target.GetComponent<ICharacter>();

            if (character != null && !character.IsInvincable)
            {
                if (TargetCanBeDamaged && !character.IsDead)
                {
                    character.ModifyHealthBy(damage);
                }
            }
        }
        private void SpawnFireball()
        {
            GameObject spawnedObject = Instantiate(fireball, transform.position + new Vector3(enemyControler.DirectionToPlayer.x * 0.5f, 1.5f, 0), transform.rotation);

            Fireball fireballComponent = spawnedObject.GetComponent<Fireball>();
            fireballComponent.RotateFireball(enemyControler.DirectionToPlayer);
        }
        private void PickAttackType()
        {
            if (enemyAnimationManager.GetFireBlastCharging()) { enemyAnimationManager.SetAttackNumber(MyEnums.EnemyAttack.FireBlast); }
            else if (enemyControler.DistanceToPlayerX >= fireballXDistance) { enemyAnimationManager.SetAttackNumber(MyEnums.EnemyAttack.Fireball); }
            else if (enemyControler.DistanceToPlayerY >= YDistanceMax) { enemyAnimationManager.SetAttackNumber(MyEnums.EnemyAttack.JumpAttack); }
            else if (enemyControler.DistanceToPlayerY >= YDistanceMin && enemyControler.DistanceToPlayerY < YDistanceMax) { enemyAnimationManager.SetAttackNumber(MyEnums.EnemyAttack.Fireball); }
            else if (enemyControler.DistanceToPlayerX > fireDashXDistanceMin && enemyControler.DistanceToPlayerX < fireDashXDistanceMax) { enemyAnimationManager.SetAttackNumber(MyEnums.EnemyAttack.FireDashAttack); }
            else { enemyAnimationManager.SetAttackNumber(MyEnums.EnemyAttack.NormalAttack); }
        }

        public void SetTargetCanBeDamaged(bool value)
        {
            TargetCanBeDamaged = value;
        }
        public void SetCurrentAttackData(EnemyMeleeAttack attack)
        {
            CurrentAttack = attack;
        }
        public void SetEnemyIsAttacking(bool value)
        {
            EnemyIsAttacking = value;
        }
    }
}
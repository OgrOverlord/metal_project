﻿using UnityEngine;

namespace MetalProject.Characters
{
    public class BehaviorEnemyTakingDamage : StateMachineBehaviour
    {
        private EnemyControler enemyControler;
        private EnemyAnimationManager enemyAnimationManager;
        private bool componentsAreSet = false;

        private void InitialComponentSetup(Animator animator)
        {
            enemyControler = animator.gameObject.GetComponent<EnemyControler>();
            enemyAnimationManager = animator.gameObject.GetComponent<EnemyAnimationManager>();
            componentsAreSet = true;
        }
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!componentsAreSet) { InitialComponentSetup(animator); }
            enemyAnimationManager.SetFigthing(false);
            enemyControler.SetEnemyRecovered(false);
        }
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            enemyControler.SetEnemyRecovered(true);
            enemyControler.SetGravityAffectsEnemy(true);
        }
    }
}

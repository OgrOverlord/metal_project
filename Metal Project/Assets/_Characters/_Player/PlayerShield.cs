﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.CommonUtilty;

namespace MetalProject.Combat
{
    public class PlayerShield : MonoBehaviour
    {
        [SerializeField] [Range(-20,0)] private int blockCost;
        [SerializeField] private LayerMask shieldProtectsAganist;

        private PlayerAnimationManager playerAnimationManager;

        public delegate void OnPlayerAttackBlockedDelegate(int blockCost);
        public OnPlayerAttackBlockedDelegate OnPlayerAttackBlocked;

        void Start()
        {
            playerAnimationManager = GetComponentInParent<PlayerAnimationManager>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (HelperMethods.CheckIfLayerIsInLayerMask(collision.gameObject.layer, shieldProtectsAganist)) 
            {
                //TODO think how not to use 2 getcomponent calls here
                if (collision.GetComponentInParent<EnemyAnimationManager>() != null &&   
                    !collision.GetComponentInParent<EnemyCombatControler>().CurrentAttack.unblockable)
                {
                    playerAnimationManager.SetEnemyAttackBlocked();
                    OnPlayerAttackBlocked(blockCost);
                    EnemyAnimationManager enemyAnimationManager = collision.GetComponentInParent<EnemyAnimationManager>();
                    enemyAnimationManager.SetAttackBlocked();
                }

                if (collision.GetComponent<Fireball>())
                {
                    playerAnimationManager.SetEnemyAttackBlocked();
                    OnPlayerAttackBlocked(blockCost);
                }
            }
        }
    }
}
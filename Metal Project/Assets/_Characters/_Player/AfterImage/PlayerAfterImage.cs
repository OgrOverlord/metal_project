﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetalProject.Characters
{
    public class PlayerAfterImage : MonoBehaviour
    {
        [SerializeField] [Range(0f, 1f)] private float activeTime;
        [SerializeField] private Color colorTint;
        [SerializeField] private float initialAlpha;
        [SerializeField] private float alphaDecay;

        private float alpha;
        private float timeActivated;
        private Color color;

        private Player player;
        private SpriteRenderer spriteRenderer;
        private SpriteRenderer playerSpriteRender;

        void OnEnable()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            player = FindObjectOfType<Player>();
            playerSpriteRender = player.GetComponent<SpriteRenderer>();

            alpha = initialAlpha;
            spriteRenderer.sprite = playerSpriteRender.sprite;
            transform.position = player.transform.position;
            transform.rotation = player.transform.rotation;
            transform.localScale = player.transform.localScale;
            timeActivated = Time.time;
        }

        void Update()
        {
            alpha -= alphaDecay * Time.deltaTime;
            spriteRenderer.color = new Color(colorTint.r, colorTint.g, colorTint.b, alpha);
            if (Time.time >= (timeActivated + activeTime)) { PlayerAfterImagePool.Instance.AddToPool(gameObject); }
        }

    }
}

﻿using MetalProject.Characters;
using UnityEngine;

namespace MetalProject.Combat
{
    public class RangeAttack_Behavior : StateMachineBehaviour
    {

        [SerializeField] [Range(0, 10)] private float pushbackDistanceX;
        [SerializeField] [Range(0, 2)] private float pushbackDuration;

        private PlayerControler playerControler;
        private PlayerAnimationManager playerAnimationManager;

        private Vector2 pushBackVector;
        private float movementStartTime;
        private bool gunFired;

        void Awake()
        {
            playerControler = FindObjectOfType<PlayerControler>();
            playerAnimationManager = FindObjectOfType<PlayerAnimationManager>();
            playerAnimationManager.OnFireWeapon += SetMovmentStartTime;
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            playerControler.SetGravityAffectsPlayer(false);

            pushBackVector = playerControler.MovmentDirection > 0 ?
                new Vector2(-playerControler.MovmentDirection * pushbackDistanceX, 0) :
                new Vector2(playerControler.MovmentDirection * pushbackDistanceX, 0);
        }
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (gunFired) { playerControler.SetAttackMovmentVelocity(pushBackVector, pushbackDuration); }
            if (gunFired && movementStartTime + pushbackDuration <= Time.time) { playerControler.SetAttackMovmentVelocity(new Vector2(0, 0)); }
        }
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            playerControler.SetGravityAffectsPlayer(true);
            gunFired = false;
        }

        private void SetMovmentStartTime()
        {
            gunFired = true;
            movementStartTime = Time.time;
        }
    }
}
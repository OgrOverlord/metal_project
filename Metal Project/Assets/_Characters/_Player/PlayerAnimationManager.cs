﻿using UnityEngine;
using MetalProject.Combat;
using MetalProject.CommonUtilty;

namespace MetalProject.Characters
{
    public class PlayerAnimationManager : MonoBehaviour
    {

        Animator animator;
        SpriteRenderer spriteRenderer;
        AnimationParameters animParameters;
        PlayerCombatControler playerCombatControler;

        //TODO find a way to automate this process by using the Unity Editor Scripting 
        private struct AnimationParameters
        {
            public int Running;
            public int Jump;
            public int Second_Jump;
            public int Airborn;
            public int Stomp_Start;
            public int Stomp;
            public int Start_Grabbing;
            public int Grabbing;
            public int Dashing;
            public int Range_Attack;
            public int Blocking;
            public int EnemyAttackBlocked;
            public int PlayerAttackBlocked;
            public int Taking_Damage;
            public int Dying;
            public int Charging_Attack;
            public int Respawn;
        }


        public delegate void OnFireWeaponDelegate();
        public event OnFireWeaponDelegate OnFireWeapon;

        public delegate void OnPlayerChargedAttackDelegate (MetalEventArgs args);
        public event OnPlayerChargedAttackDelegate OnPlayerChargedAttack;

        void Awake()
        {
            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            animParameters = new AnimationParameters
            {
                Running = Animator.StringToHash("Running"),
                Jump = Animator.StringToHash("Jump"),
                Second_Jump = Animator.StringToHash("Second_Jump"),
                Airborn = Animator.StringToHash("Airborn"),
                Stomp_Start = Animator.StringToHash("Stomp_Start"),
                Stomp = Animator.StringToHash("Stomp"),
                Start_Grabbing = Animator.StringToHash("Start_Grabbing"),
                Grabbing = Animator.StringToHash("Grabbing"),
                Dashing = Animator.StringToHash("Dashing"),
                Range_Attack = Animator.StringToHash("Range_Attack"),
                Blocking = Animator.StringToHash("Blocking"),
                EnemyAttackBlocked = Animator.StringToHash("EnemyAttackBlocked"),
                PlayerAttackBlocked = Animator.StringToHash("PlayerAttackBlocked"),
                Taking_Damage = Animator.StringToHash("Taking_Damage"),
                Dying = Animator.StringToHash("Dying"),
                Charging_Attack = Animator.StringToHash("Charging_Attack"),
                Respawn = Animator.StringToHash("Respawn"),
            };
        }
      
        #region Trigger and Bool control methods 
        public void SetRunning(bool value)
        {
            animator.SetBool(animParameters.Running, value);
        }

        public void SetPlayerJump()
        {
            animator.SetTrigger(animParameters.Jump);
        }
        public void SetPlayerSecondJump()
        {
            animator.SetTrigger(animParameters.Second_Jump);
        }

        public void SetAirborn(bool collisionBelow)
        {
            animator.SetBool(animParameters.Airborn, collisionBelow);
        }
        public bool GetAirborn()
        {
            return animator.GetBool(animParameters.Airborn);
        }

        public void SetStartStomping()
        {
            animator.SetTrigger(animParameters.Stomp_Start);
        }
        public void SetStomp()
        {
            animator.SetTrigger(animParameters.Stomp);
        }

        public void SetStartGrabbing()
        {
            animator.SetTrigger(animParameters.Start_Grabbing);
        }
        public void SetGrabbing(bool grabbing)
        {
            animator.SetBool(animParameters.Grabbing, grabbing);
        }
        public bool GetGrabbing()
        {
            return animator.GetBool(animParameters.Grabbing);
        }

        public void SetDashing(bool dashing)
        {
            animator.SetBool(animParameters.Dashing, dashing);
        }
        public bool GetDashing()
        {
            return animator.GetBool(animParameters.Dashing);
        }

        public void SetAttackFlag(MeleeAttack attackData, bool value)
        {
            animator.SetBool(attackData.name, value);
        }
        public void SetChargingAttack(bool value)
        {
            animator.SetBool(animParameters.Charging_Attack, value);
        }
        public bool GetChargingAttack()
        {
            return animator.GetBool(animParameters.Charging_Attack);
        }

        public void SetBlocking(bool value)
        {
            animator.SetBool(animParameters.Blocking, value);
        }
        public bool GetBlocking()
        {
            return animator.GetBool(animParameters.Blocking);
        }
        public void SetEnemyAttackBlocked()
        {
            animator.SetTrigger(animParameters.EnemyAttackBlocked);
        }
        public void SetPlayerAttackBlocked()
        {
            animator.SetTrigger(animParameters.PlayerAttackBlocked);
        }

        public void SetRangeAttack()
        {
            animator.SetTrigger(animParameters.Range_Attack);
        }
        public void SetTakingDamage()
        {
            animator.SetTrigger(animParameters.Taking_Damage);
        }

        public void SetDying()
        {
            animator.SetTrigger(animParameters.Dying);
        }
        public void SetRespawn()
        {
            animator.SetTrigger(animParameters.Respawn);
        }
        #endregion

        #region Methods raised by animation events

        public void PassFireWeaponEvent() // passes animation event to method on diffrent gameobject  
        {
            OnFireWeapon();
        }

        public void ShakeScreen()
        {
            OnPlayerChargedAttack(new MetalEventArgs(0.25f, 1f, 0.5f));
        }

        #endregion


    }
}




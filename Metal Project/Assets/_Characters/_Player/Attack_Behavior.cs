﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.Core;


namespace MetalProject.Combat
{
    public class Attack_Behavior : StateMachineBehaviour
    {
        [SerializeField] private MeleeAttack attackData;

        private PlayerAnimationManager playerAnimationManager;
        private PlayerCombatControler playerCombatControler;
        private PlayerControler playerControler;

        private bool nextAttackIsSet;
        private bool attackWindowMissed;
        private bool attackVelocitySet;

        private float movementStartTime;
        private float attackAnimationStartTime;

        void Awake()
        {
            playerCombatControler = FindObjectOfType<PlayerCombatControler>();
            playerControler = FindObjectOfType<PlayerControler>();
            playerAnimationManager = FindObjectOfType<PlayerAnimationManager>();
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            playerCombatControler.SetCurrentAttack(attackData);
            playerCombatControler.SetPlayerIsAttacking(true);
            playerCombatControler.ResetAttackButton();
            attackVelocitySet = false;
            attackAnimationStartTime = Time.time;
        }
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!attackVelocitySet && attackData.attackHasMovment && attackAnimationStartTime + attackData.movmentDelay <= Time.time) { ApplyAttackMovmentVelocity(); }
            if (movementStartTime + attackData.movementDuration <= Time.time) { playerControler.SetAttackMovmentVelocity(new Vector2(0, 0)); } //reset velocity after defined movment duration
            if (attackData.IsAirborn) { playerControler.SetGravityAffectsPlayer(false); }
            if (attackData.nextAttacks.Length == 0) { RestCurrentAttackFlag(); }
            if (attackData.nextAttacks.Length > 0)
            {
                SetFollowupAttack();
                if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= stateInfo.length && !nextAttackIsSet) { RestCurrentAttackFlag(); } //Attack ended before next one was launched
            }

            playerAnimationManager.SetAttackFlag(playerCombatControler.firstAirAttack, false); //Note: added to prevent 4th air attack after combo was performed. Case when attacks are spammed

        }
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            nextAttackIsSet = false;
            playerAnimationManager.SetAttackFlag(attackData, false);
            playerCombatControler.ResetAttackButton();
            playerCombatControler.SetPlayerIsAttacking(false);
            playerControler.SetGravityAffectsPlayer(true);
        }

        private void ApplyAttackMovmentVelocity()
        {
            if ((attackData.UpReq && playerControler.GetCurrentDirectionalInput().y > 0) ||
                   (attackData.DownReq && playerControler.GetCurrentDirectionalInput().y < 0) ||
                   (attackData.RightReq && playerControler.GetCurrentDirectionalInput().x > 0) ||
                   (attackData.LeftReq && playerControler.GetCurrentDirectionalInput().x < 0))
            {
                attackVelocitySet = true;
                movementStartTime = Time.time;
                playerControler.SetAttackMovmentVelocity(attackData.attackMoveDistance, attackData.movementDuration);
            }
            else if (!attackData.UpReq && !attackData.DownReq && !attackData.LeftReq && !attackData.RightReq)
            {
                attackVelocitySet = true;
                movementStartTime = Time.time;
                playerControler.SetAttackMovmentVelocity(attackData.attackMoveDistance, attackData.movementDuration);
            }
        }
        private void RestCurrentAttackFlag()
        {
            playerAnimationManager.SetAttackFlag(attackData, false);
            if (attackData.IsAirborn) { playerCombatControler.SetAirComboPerformed(true); }
        }
        private void SetFollowupAttack()
        {
            if (!nextAttackIsSet && playerCombatControler.AttackButtonPressed)
            {
                foreach (var nextAttack in attackData.nextAttacks)
                {
                    if ((Time.time - playerCombatControler.LastClickTime) > (nextAttack.attackWindowPercantage.x * attackData.animationClip.length)
                    && (Time.time - playerCombatControler.LastClickTime) < nextAttack.attackWindowPercantage.y * attackData.animationClip.length)
                    {
                        if (!playerCombatControler.AirComboPerformed && attackData.IsAirborn) //Set next attack if air combo was not perffomed 
                        {
                            playerAnimationManager.SetAttackFlag(nextAttack, true);
                            nextAttackIsSet = true;
                        }

                        if (!attackData.IsAirborn) //Set next ground attack
                        {
                            playerAnimationManager.SetAttackFlag(nextAttack, true);
                            nextAttackIsSet = true;
                        }
                    }
                }
            }
        }
    }
}
﻿using UnityEngine;
using MetalProject.Core;

namespace MetalProject.Characters
{
    public class PlayerHearingArea : MonoBehaviour
    {
        Collider2D colliderArea;

        void Start()
        {
            colliderArea = GetComponent<Collider2D>();
        }

        public bool CheckIfSoundSourceIsInRange(Vector3 gameObjectPosition)
        {
            if (colliderArea.bounds.Contains(gameObjectPosition)) { return true; }
            else { return false; }
        }
    }
}
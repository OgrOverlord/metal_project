﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetalProject.Characters
{
    public class TakingDamage_Behavior : StateMachineBehaviour
    {

        private Player player;
        private bool componentsAreSet = false;

        private void InitialComponentSetup(Animator animator)
        {
            player = animator.gameObject.GetComponent<Player>();
            componentsAreSet = true;
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            InitialComponentSetup(animator);
            player.SetIsInvincable(true);
        }
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            player.SetIsInvincable(false);
        }

    }
}
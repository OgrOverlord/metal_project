﻿using UnityEngine;

namespace MetalProject.Combat
{
    [CreateAssetMenu(fileName = "New Bullet", menuName = "Attacks/Bullet")]
    public class Bullet : ScriptableObject
    {
        //This layout is override by the custom editor script 
        [Header("Bullet Layers")]
        public LayerMask layerMaskHit;
        public LayerMask layerMaskDamages;

        [Header("Bullet data")]
        public bool pierces;
        [Range(0, 8)] public int targetsToPierce;
        [Range(0f, 15f)] public float piercingRange;
        public int damage;
        public int maxNumber;
        public int startingNumber;


        [Header("Bullet presentation")]
        public GameObject bulletSplash;
        public Sprite bulletSprite;
        public AudioClip bulletSound;
    }
}
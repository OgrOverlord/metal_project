﻿using UnityEngine;

namespace MetalProject.Combat
{
    public class BulletBehavior : MonoBehaviour
    {
        public void DestroyBullet()
        {
            Destroy(this.gameObject);
        }
    }
}
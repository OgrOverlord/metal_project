﻿using UnityEngine;
using NaughtyAttributes;

namespace MetalProject.Combat
{

    [CreateAssetMenu(fileName = "New Melee Attack", menuName = "Attacks/Melee Attack")]
    public class MeleeAttack : ScriptableObject
    {
        [Header("Base Parameters")]
        [Range(-100, 0)] public int damage;
        public MeleeAttack[] nextAttacks;
        public LayerMask layerMask;
        public AnimationClip animationClip;

        [MinMaxSlider(0f, 1f)] public Vector2 attackWindowPercantage;

        [Header("Attack Movemnt Parameters")]
        public bool attackHasMovment;
        [ShowIf("attackHasMovment")] public Vector2 attackMoveDistance;
        [ShowIf("attackHasMovment")] public float movementDuration;
        [ShowIf("attackHasMovment")] public float movmentDelay;
        public bool IsAirborn;

        [Header("Parameters Affecting Enemies")]
        public Vector2 enemyPushDistance;
        public float pushDuration;
        public bool targetIsAffectedByGravity;

        [Header("Addtional Button Condtion")]
        public bool UpReq;
        public bool DownReq;
        public bool LeftReq;
        public bool RightReq;
    }
}
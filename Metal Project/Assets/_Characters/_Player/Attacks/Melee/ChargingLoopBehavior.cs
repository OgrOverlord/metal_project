﻿using UnityEngine;

namespace MetalProject.Combat
{
    public class ChargingLoopBehavior : StateMachineBehaviour
    {
        private PlayerCombatControler playerCombatControler;

        void Awake()
        {
            playerCombatControler = FindObjectOfType<PlayerCombatControler>();
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            playerCombatControler.SetChargedAttackLoopReached(true);
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            playerCombatControler.SetChargedAttackLoopReached(false);
        }
    }
}

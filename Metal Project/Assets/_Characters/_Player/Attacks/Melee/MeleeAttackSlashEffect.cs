﻿using UnityEngine;


namespace MetalProject.Combat
{
    public class MeleeAttackSlashEffect : MonoBehaviour
    {
        public void DestroySplash()
        {
            Destroy(this.gameObject);
        }
    }
}

﻿using UnityEngine;
using MetalProject.Characters;
using MetalProject.Core;
using MetalProject.CommonUtilty;

namespace MetalProject.Combat
{
    public class PlayerCombatControler : MonoBehaviour
    {
        #region Serialized fields
        [Header("Melee Attack Properties")]
        [SerializeField] public MeleeAttack firstGroundAttack;
        [SerializeField] public MeleeAttack firstAirAttack;
        [SerializeField] public MeleeAttack chargedAttack;
        [SerializeField] MeleeAttackSlashEffect[] meleeAttackSlashEffects;
        [SerializeField] AudioClip[] meleeAttackHitSounds;
        [SerializeField] [Range(0, 1f)] float meleeAttackHitSoundsVolume;

        [Header("Ranged Attack Properties")]
        [SerializeField] Transform bulletSpawnTransform;
        [SerializeField] Bullet[] playerBulletTypes;
        [SerializeField] float rangedAttackRange;
        [SerializeField] AudioClip[] outOfAmmoVoice;

        #endregion

        #region Properties
        public Bullet ActiveBulletType { get; private set; }
        public Bullet[] PlayerBulletTypes
        {
            get { return playerBulletTypes; }
            private set { playerBulletTypes = value; }
        }
        public int[] AvailableBullets { get; private set; }
        public MeleeAttack CurrentAttack { get; private set; }

        public bool AirComboPerformed { get; private set; }
        public bool PlayerIsAttacking { get; private set; }
        public bool ChargedAttackLoopReached { get; private set; }
        public bool AttackButtonPressed { get; private set; }
        public float LastClickTime { get; private set; }
        public float ClickTime { get; private set; }
        public int NumberOfClicks { get; private set; }

        #endregion

        #region Fields
        PlayerAnimationManager playerAnimationManager;
        PlayerControler playerControler;
        Player player;
        GameManager gameManager;
        AudioManager audioManager;

        Vector3 bulletSpawnOriginVector;
        float rangeAttackDirection;
        int activeBulletIndex;

        //float[] clickTimes = new float[3];
        //float comboDelay = 1f;

        #endregion

        #region Events
        public delegate void OnSwitchBulletTypeDelegate(int activeBulletIndex);
        public event OnSwitchBulletTypeDelegate OnSwitchBulletType;

        public delegate void OnNumberOFBulletsReducedDelegate(int currentNumberOfBullets, bool updateBulletText);
        public event OnNumberOFBulletsReducedDelegate OnNumberOFBulletsReduced;
        #endregion

        void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
            audioManager = FindObjectOfType<AudioManager>();

            gameManager.GameInputMaster.Player.Block.performed += ctx => Block();
            gameManager.GameInputMaster.Player.RangeAttack.performed += ctx => RangeAttack();
            gameManager.GameInputMaster.Player.SwitchAmmoType.performed += ctx => SwitchBulletType();
            gameManager.GameInputMaster.Player.MeleeAttack.performed += ctx => MeleeAttack();
            gameManager.GameInputMaster.Player.HeavyMeleeAttack.performed += ctx => StartChargingMeleeAttack();
            gameManager.GameInputMaster.Player.CancleHeavyMeleeAttack.performed += ctx => CancleOrPerformleChargedMeleeAttack();

            playerControler = GetComponentInParent<PlayerControler>();
            playerAnimationManager = GetComponentInParent<PlayerAnimationManager>();
            player = GetComponentInParent<Player>();
            playerAnimationManager.OnFireWeapon += FireBullet;

            SetupPlayerBullets();
            LoadPlayerData();
        }
        void Start()
        {
            ClickTime = 0f;
        }
        void Update()
        {
            UpdateGunRayOrigin();
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == HelperMethods.LayerMaskToLayer(CurrentAttack.layerMask))
            {
                if (collision.GetComponent<ICharacter>() != null)
                {
                    ICharacter target = collision.GetComponent<ICharacter>();

                    if (!target.IsInvincable)
                    {
                        audioManager.PlaySFXAtVolume(meleeAttackHitSounds[Random.Range(0, meleeAttackHitSounds.Length - 1)], meleeAttackHitSoundsVolume);
                        SpawnAttakSplash(collision, target);
                        DealDamageAndReduceTargetStamina(CurrentAttack.damage, target);

                        if (CurrentAttack.enemyPushDistance != Vector2.zero && !target.IsInvincable)
                        {
                            PushEnemy(CurrentAttack.enemyPushDistance, CurrentAttack.pushDuration, playerControler.MovmentDirection, collision);
                        }
                    }
                    else if (!target.IsDead)
                    {
                        playerAnimationManager.SetPlayerAttackBlocked();
                        playerControler.SetAttackMovmentVelocity(new Vector2(-10, 0)); //TODO magic numbers man, deal with this
                    }
                    if (target is Enemy)
                    {
                        LockEnemyInAir(collision);
                    }
                }
            }
        }


        #region private methods

        private void LoadPlayerData()
        {
            if (PlayerData.NormalBullet != -1 && PlayerData.PiercingBullets != -1) //TODO probably should be managed better than int field for every bullet type
            {
                AvailableBullets[0] = PlayerData.NormalBullet;
                AvailableBullets[1] = PlayerData.PiercingBullets;
            }
        }

        private void MeleeAttack()
        { 
                if (NumberOfClicks >= 3) { NumberOfClicks = 0; }

                AttackButtonPressed = true;
                LastClickTime = ClickTime;
                ClickTime = Time.time;
                NumberOfClicks++;

                //clickTimes[NumberOfClicks - 1] = ClickTime;

                if (playerAnimationManager.GetAirborn())
                {
                    if (!PlayerIsAttacking && !AirComboPerformed) { playerAnimationManager.SetAttackFlag(firstAirAttack, true); }
                }
                else
                {
                    if (!PlayerIsAttacking) { playerAnimationManager.SetAttackFlag(firstGroundAttack, true); }
                }
        }
        private void StartChargingMeleeAttack()
        {
            if (!playerAnimationManager.GetAirborn() && !PlayerIsAttacking)
            {
                playerAnimationManager.SetChargingAttack(true);
                PlayerIsAttacking = true;
            }
        }
        private void CancleOrPerformleChargedMeleeAttack()
        {
            playerAnimationManager.SetChargingAttack(false);

            if (!ChargedAttackLoopReached) { PlayerIsAttacking = false; }
            else { playerAnimationManager.SetAttackFlag(chargedAttack, true); }
        }
        private void SpawnAttakSplash(Collider2D collision, ICharacter target)
        {
            if (target is Enemy)
            {
                Instantiate(meleeAttackSlashEffects[0], new Vector3(collision.bounds.center.x, collision.bounds.center.y), Quaternion.identity, collision.transform);
            }
            else if (target is Boss)
            {
                Instantiate(meleeAttackSlashEffects[0],
                            new Vector3(collision.bounds.center.x, this.transform.position.y + 0.5f),
                            Quaternion.identity, collision.transform);
            }
        }

        private void Block()
        {
            if (!playerAnimationManager.GetBlocking())
            {
                if (player.CurentStamina > 0)
                {
                    playerAnimationManager.SetBlocking(true);
                    player.ModifyStaminaRechargeRate(0);
                }
            }
            else
            {
                playerAnimationManager.SetBlocking(false);
                player.ResetStaminaRechargeRate();
            }
        }

        private void SetupPlayerBullets() //TODO consider calling this method only once 
        {
            activeBulletIndex = 0;
            ActiveBulletType = PlayerBulletTypes[activeBulletIndex]; //TODO in future find way to easily give player more bullet types during gameplay (unlocking)
            AvailableBullets = new int[PlayerBulletTypes.Length];

            for (int i = 0; i < PlayerBulletTypes.Length; i++)
            {
                AvailableBullets[i] = PlayerBulletTypes[i].startingNumber;
            }
        }
        private void RangeAttack()
        {
            rangeAttackDirection = playerControler.MovmentDirection;

            if (AvailableBullets[activeBulletIndex] > 0) { playerAnimationManager.SetRangeAttack(); }
            else
            {
                audioManager.PlayDialog(outOfAmmoVoice[Random.Range(0, outOfAmmoVoice.Length)]);
            }
        }
        private void UpdateGunRayOrigin()
        {
            bulletSpawnOriginVector.x = bulletSpawnTransform.transform.position.x;
            bulletSpawnOriginVector.y = bulletSpawnTransform.transform.position.y;
        }
        private RaycastHit2D CastGunRay()
        {
            RaycastHit2D Hit = Physics2D.Raycast(bulletSpawnOriginVector, Vector2.right * rangeAttackDirection, rangedAttackRange, ActiveBulletType.layerMaskHit);
            Debug.DrawRay(bulletSpawnOriginVector, Vector2.right * rangedAttackRange * rangeAttackDirection, Color.green, 2f);

            return Hit;
        }
        private RaycastHit2D CastGunRay(Vector2 castStartingPosition)
        {
            //TODO get rid of the magic numbers and create a proper setting of position for the raycasts
            RaycastHit2D Hit = Physics2D.Raycast(castStartingPosition + (Vector2.right * rangeAttackDirection) + Vector2.up,
                                                  Vector2.right * rangeAttackDirection, rangedAttackRange, ActiveBulletType.layerMaskHit);
            Debug.DrawRay(castStartingPosition + (Vector2.right * rangeAttackDirection) + Vector2.up,
                           Vector2.right * rangeAttackDirection * ActiveBulletType.piercingRange, Color.red, 2f);
            return Hit;
        }

        private void SwitchBulletType()
        {
            activeBulletIndex++;
            if ((PlayerBulletTypes.Length - 1) < activeBulletIndex) { activeBulletIndex = 0; }
            ActiveBulletType = PlayerBulletTypes[activeBulletIndex];
            OnSwitchBulletType(activeBulletIndex);
        }
        private void SpawnBulletSplash(RaycastHit2D target)
        {
            GameObject bulletSplash = Instantiate(ActiveBulletType.bulletSplash, new Vector3(target.point.x, target.point.y), Quaternion.identity);
            bulletSplash.gameObject.transform.localScale = new Vector3(rangeAttackDirection, 1f, 1f);
        }

        private void DealDamageAndReduceTargetStamina(int damage, ICharacter character)
        {
            if (character != null)
            {
                if (character is Enemy enemy)
                {
                    enemy.ModifyHealthBy(damage);
                    enemy.ModifyStaminaBy(damage);
                    enemy.IncrementAttacksTaken();
                }

                if (character is Boss boss)
                {
                    boss.ModifyHealthBy(damage);
                    boss.ModifyStaminaBy(damage);
                }
            }
        }
        private void PushEnemy(Vector2 distance, float pushDuration, float pushDirection, Collider2D target)
        {
            EnemyControler enemyControler = target.GetComponent<EnemyControler>();
            if (enemyControler != null) { enemyControler.SetPushVelocity(distance, pushDirection, pushDuration, CurrentAttack.name); }
        }
        private void LockEnemyInAir(Collider2D target)
        {
            EnemyControler enemyControler = target.GetComponent<EnemyControler>();
            enemyControler.SetGravityAffectsEnemy(CurrentAttack.targetIsAffectedByGravity);
        }
        #endregion

        #region Raised by Animation event 
        public void FireBullet()
        {
            ChangeNumberOfBullets(-1, activeBulletIndex, true);
            RaycastHit2D target = CastGunRay();
            AudioManager.Instance.PlaySFX(ActiveBulletType.bulletSound);

            if (target)
            {
                SpawnBulletSplash(target);

                if (HelperMethods.CheckIfLayerIsInLayerMask(target.transform.gameObject.layer, ActiveBulletType.layerMaskDamages)
                    && !target.transform.gameObject.GetComponent<ICharacter>().IsInvincable)
                {
                    DealDamageAndReduceTargetStamina(ActiveBulletType.damage, target.collider.GetComponent<ICharacter>());
                }

                if (ActiveBulletType.pierces)
                {
                    for (int i = 1; i <= ActiveBulletType.targetsToPierce; i++)
                    {
                        if (target)
                        {
                            target = CastGunRay(target.transform.position);
                            SpawnBulletSplash(target);

                            if (target)
                            {
                                if (HelperMethods.CheckIfLayerIsInLayerMask(target.transform.gameObject.layer, ActiveBulletType.layerMaskDamages))
                                {
                                    DealDamageAndReduceTargetStamina(ActiveBulletType.damage, target.collider.GetComponent<ICharacter>());
                                }
                                else
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Public Methods 
        public void SetPlayerIsAttacking(bool value)
        {
            PlayerIsAttacking = value;
        }
        public void SetChargedAttackLoopReached(bool value)
        {
            ChargedAttackLoopReached = value;
        }
        public void SetAirComboPerformed(bool value)
        {
            AirComboPerformed = value;
        }
        public void SetCurrentAttack(MeleeAttack attackData)
        {
            CurrentAttack = attackData;
        }
        public void ResetAttackButton()
        {
            AttackButtonPressed = false;
        }

        public void ChangeNumberOfBullets(int value, int activeBulletIndex, bool updateBulletText)
        {
            AvailableBullets[activeBulletIndex] += value;

            if (AvailableBullets[activeBulletIndex] > ActiveBulletType.maxNumber) { AvailableBullets[activeBulletIndex] = ActiveBulletType.maxNumber; }
            OnNumberOFBulletsReduced(AvailableBullets[activeBulletIndex], updateBulletText);
        }
        #endregion







    }
}

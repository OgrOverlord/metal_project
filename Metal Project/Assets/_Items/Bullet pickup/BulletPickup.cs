﻿using UnityEngine;
using MetalProject.Combat;

namespace MetalProject.Items
{
    public class BulletPickup : Item
    {
        [SerializeField] Bullet bulletType;

        protected override void Pickup(Collider2D collision)
        {
            PlayerCombatControler playerCombatControler = collision.gameObject.GetComponentInChildren<PlayerCombatControler>();
            for (int i = 0; i < playerCombatControler.PlayerBulletTypes.Length; i++)
            {
                if (bulletType == playerCombatControler.PlayerBulletTypes[i])
                {
                    if (bulletType == playerCombatControler.ActiveBulletType) { playerCombatControler.ChangeNumberOfBullets(amount, i, true); }
                    else { playerCombatControler.ChangeNumberOfBullets(amount, i, false); }
                }
            }
            base.Pickup(collision);
        }
    }
}
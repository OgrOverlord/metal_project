﻿using MetalProject.Characters;
using UnityEngine;

namespace MetalProject.Items
{
    public class HealthPickup : Item
    {
        protected override void Pickup(Collider2D collision)
        {
            if (collision.GetComponent<ICharacter>() != null)
            {
                ICharacter character = collision.GetComponent<ICharacter>();
                character.ModifyHealthBy(amount);
            }

            base.Pickup(collision);
        }
    }
}
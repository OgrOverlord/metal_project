﻿using UnityEngine;
using MetalProject.Characters;

namespace MetalProject.Items
{
    public class PickupPiniataController : MonoBehaviour
    {
        [SerializeField] GameObject[] itemsDroped;
        [SerializeField] Vector2[] itemDropChance;

        private Enemy enemy;

        void Start()
        {
            if (GetComponentInParent<Enemy>())
            {
                enemy = GetComponentInParent<Enemy>();
                enemy.OnEnemyDies += SpawnPickup;
            }

            for (int i = 0; i < itemDropChance.Length; i++)
            {
                itemDropChance[i].x = 1 - itemDropChance[i].x;
                itemDropChance[i].y = 1 - itemDropChance[i].y;

               // Debug.Log("X: " + itemDropChance[i].x + " -- Y: " + itemDropChance[i].y);
            }
        }

        private void SpawnPickup(Enemy enemy)
        {
            float random = Random.Range(0f, 1f);

            for (int i = 0; i < itemDropChance.Length; i++)
            {
                if (itemDropChance[i].x >= random && itemDropChance[i].y <= random)
                {
                    Instantiate(itemsDroped[i], transform.position + new Vector3(0f, 1f), transform.rotation);
                    break;
                }

            }
        }

    }
}
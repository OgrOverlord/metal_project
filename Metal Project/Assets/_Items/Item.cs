﻿using UnityEngine;
using MetalProject.Core;
using MetalProject.CommonUtilty;

namespace MetalProject.Items
{
    public abstract class Item : MonoBehaviour
    {
        [SerializeField] protected LayerMask pickupMask;
        [SerializeField] protected int amount;
        [SerializeField] protected float pickupDelay = 1.5f;
        [SerializeField] protected AudioClip pickupSound;
        [SerializeField] protected ParticleSystem pickupVFX;
    
        protected float spawnTime;
        protected AudioManager audioManager;

        void Start()
        {
            spawnTime = Time.time;
            audioManager = FindObjectOfType<AudioManager>();
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == HelperMethods.LayerMaskToLayer(pickupMask))
            {
                if (spawnTime + pickupDelay <= Time.time) { Pickup(collision); }
            }
        }

        protected virtual void Pickup(Collider2D collision)
        {
            audioManager.PlaySFX(pickupSound);
            Destroy(this.gameObject);
        }
    }
}